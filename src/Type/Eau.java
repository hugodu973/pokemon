package Type;

import java.util.HashMap;

public class Eau extends Type {
	
	public Double getReduc(Type type) {
		HashMap<String, Double> dico = new HashMap<String, Double>();
		dico.put("acier", 1.);
		dico.put("combat", 1.);
		dico.put("eau", 0.5);
		dico.put("electricite", 1.);
		dico.put("feu", 2.);
		dico.put("normal", 1.);
		dico.put("plante", 0.5);
		dico.put("roche", 2.);
		dico.put("sol", 2.);
		dico.put("vol", 1.);
		dico.put("poison", 1.);
		dico.put("psy", 1.);
		
		return dico.get(type.getClass().toString().split("\\.")[1].toLowerCase());
	}

}
