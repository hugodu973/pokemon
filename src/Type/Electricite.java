package Type;

import java.util.HashMap;

public class Electricite extends Type {
	
	public Double getReduc(Type type) {
		HashMap<String, Double> dico = new HashMap<String, Double>();
		dico.put("acier", 1.);
		dico.put("combat", 1.);
		dico.put("eau", 2.);
		dico.put("electricite", 0.5);
		dico.put("feu", 1.);
		dico.put("normal", 1.);
		dico.put("plante", 0.5);
		dico.put("roche", 1.);
		dico.put("sol", 0.);
		dico.put("vol", 2.);
		dico.put("poison", 1.);
		dico.put("psy", 1.);
		
		return dico.get(type.getClass().toString().split("\\.")[1].toLowerCase());
	}

}