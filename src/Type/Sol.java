package Type;

import java.util.HashMap;

public class Sol extends Type {
	
	public Double getReduc(Type type) {
		HashMap<String, Double> dico = new HashMap<String, Double>();
		dico.put("acier", 2.);
		dico.put("combat", 1.);
		dico.put("eau", 0.5);
		dico.put("electricite", 2.);
		dico.put("feu", 2.);
		dico.put("normal", 1.);
		dico.put("plante", 0.5);
		dico.put("roche", 2.);
		dico.put("sol", 1.);
		dico.put("vol", 0.);
		dico.put("poison", 2.);
		dico.put("psy", 1.);
		
		return dico.get(type.getClass().toString().split("\\.")[1].toLowerCase());
	}

}