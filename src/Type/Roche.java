package Type;

import java.util.HashMap;

public class Roche extends Type {
	
	public Double getReduc(Type type) {
		HashMap<String, Double> dico = new HashMap<String, Double>();
		dico.put("acier", 0.5);
		dico.put("combat", 0.5);
		dico.put("eau", 1.);
		dico.put("electricite", 1.);
		dico.put("feu", 2.);
		dico.put("normal", 1.);
		dico.put("plante", 1.);
		dico.put("roche", 1.);
		dico.put("sol", 0.5);
		dico.put("vol", 2.);
		dico.put("poison", 1.);
		dico.put("psy", 1.);
		
		return dico.get(type.getClass().toString().split("\\.")[1].toLowerCase());
	}

}