package Type;

import java.util.HashMap;

public class Combat extends Type {
	
	public Double getReduc(Type type) {
		HashMap<String, Double> dico = new HashMap<String, Double>();
		dico.put("acier", 2.);
		dico.put("combat", 1.);
		dico.put("eau", 1.);
		dico.put("electricite", 1.);
		dico.put("feu", 1.);
		dico.put("normal", 2.);
		dico.put("plante", 1.);
		dico.put("roche", 2.);
		dico.put("sol", 1.);
		dico.put("vol", 0.5);
		dico.put("poison", 0.5);
		dico.put("psy", 0.5);
		
		return dico.get(type.getClass().toString().split("\\.")[1].toLowerCase());
	}

}