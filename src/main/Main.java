package main;

import java.util.ArrayList;

import InterfaceGraphique.FenetrePrincipale;
import controleur.Controleur;
import deplacement.Grille;
import perso.Dresseur;
import perso.Joueur;
import perso.Personnage;
import pokemon.PokeDresse;
import pokemon.Pokemon;

public class Main {

	public static void main(String[] args) {


		//creation de la carte
		Grille grille = new Grille();
		
		//creation du joueur et positionnement
		Joueur p = new Joueur(0, "Joueur", grille.getCases().get(100).get(100));

		//initialisation de l'equipe du joueur
		Pokemon salameche = new PokeDresse("salameche", 3);
		p.getEquipe().add(salameche);
		Pokemon carapuce = new PokeDresse("carapuce", 3);
		p.getEquipe().add(carapuce);
		Pokemon pikachu = new PokeDresse("pikachu",3);
		p.getEquipe().add(pikachu);
		//pokemon par defaut
		p.setPokemonParDefaut(salameche);
		
		
		ArrayList<Personnage> lNPC = new ArrayList<Personnage>();
		//creation des PNJ(dresseurs)
		Dresseur paul = new Dresseur(0,"Paul",grille.getCases().get(90).get(85));
		grille.getCases().get(90).get(85).setPersonnage(paul);

		lNPC.add(paul);
		
		System.out.println(grille.getCases().get(90).get(85).getPersonnage() instanceof Dresseur);
		Controleur control = new Controleur(grille,p,lNPC);
		
		FenetrePrincipale fp = new FenetrePrincipale(control);
		
	}
}