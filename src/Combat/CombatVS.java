package Combat;
import perso.Dresseur;
import perso.Joueur;
import pokemon.Pokemon;

public class CombatVS {
	
	private boolean combatFini;
	private Pokemon combattant1;
	private Pokemon combattant2;
	private Joueur proprietaire1;
	private Dresseur proprietaire2;
	private int tour;
	
	public CombatVS(Pokemon combattant1, Pokemon combattant2, Joueur proprietaire1, Dresseur proprietaire2) {
		this.combatFini = false;
		this.combattant1 = combattant1;
		this.combattant2 = combattant2;
		this.proprietaire1 = proprietaire1;
		this.proprietaire2 = proprietaire2;
		this.tour = 1;
	}

	public boolean getCombatFini() {
		return combatFini;
	}

	public void setCombatFini(boolean combat) {
		this.combatFini = combat;
	}

	public Pokemon getCombattant1() {
		return combattant1;
	}

	public void setCombattant1(Pokemon combattant1) {
		this.combattant1 = combattant1;
	}

	public Pokemon getCombattant2() {
		return combattant2;
	}

	public void setCombattant2(Pokemon combattant2) {
		this.combattant2 = combattant2;
	}

	public Joueur getProprietaire1() {
		return proprietaire1;
	}

	public void setProprietaire1(Joueur proprietaire1) {
		this.proprietaire1 = proprietaire1;
	}

	public Dresseur getProprietaire2() {
		return proprietaire2;
	}

	public void setProprietaire2(Dresseur proprietaire2) {
		this.proprietaire2 = proprietaire2;
	}

	public int getTour() {
		return tour;
	}

	public void setTour(int tour) {
		this.tour = tour;
	}
	
	
}
