package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.Timer;

import Combat.CombatVS;
import InterfaceGraphique.BoutonEquipe;
import InterfaceGraphique.ChoixPanel;
import InterfaceGraphique.CombatPanel;
import InterfaceGraphique.DeplacementPanel;
import InterfaceGraphique.EquipePanel;
import InterfaceGraphique.FenetrePrincipale;
import InterfaceGraphique.InventairePokemonPanel;
import InterfaceGraphique.LoadAndSavePanel;
import InterfaceGraphique.MenuPrincipalPanel;
import InterfaceGraphique.MessagePanel;
import InterfaceGraphique.PokemonPanel;
import attaques.Att;
import attaques.AttPhysique;
import attaques.Attaque;
import attaques.Def;
import attaques.Esq;
import deplacement.Case;
import deplacement.CentreSoins;
import deplacement.Dangereuse;
import deplacement.Grille;
import inventaire.AttrapeBall;
import inventaire.Gigaball;
import inventaire.Megaball;
import inventaire.Objet;
import inventaire.Pokeball;
import inventaire.Potion1;
import inventaire.PotionSoin;
import inventaire.Superball;
import perso.Dresseur;
import perso.Joueur;
import perso.Personnage;
import perso.Soigneur;
import pokemon.PokeDresse;
import pokemon.PokeSauvage;
import pokemon.Pokemon;

/**
 * La classe controleur, qui gere les entrees utilisateurs, reagit en consequence sur le jeu et met a jour la Vue
 * @author formation
 *
 */
public class Controleur {
	private Grille grille;
	private Joueur player;
	private ArrayList<Personnage> listeNPC;
	private CombatVS combat;
	private CombatPanel combatPanel; //non definie par le constructeur car combatPanel doit connaitre le controleur qui devrait connaitre combatPanel etc...
	private FenetrePrincipale fp;
	private DeplacementPanel dp;
	private MenuPrincipalPanel mp;
	private EquipePanel equipe;
	private InventairePokemonPanel inventairePokemon;
	private ArrayList<String> listeSaves;
	private LoadAndSavePanel savePanel;
	private LoadAndSavePanel loadPanel;


	/**
	 * 
	 * @param Grille grille
	 * @param Joueur player
	 * @param ArrayList<Personnage> listeNPC
	 * 
	 */
	public Controleur(Grille grille, Joueur player, ArrayList<Personnage> listeNPC) {
		//Le reste des attributs est initialise apres coup,
		//car ils necessitent eux meme un controleur pour etre construits
		//ce qui conduirait a une boucle infinie de construction
		this.grille = grille;
		this.player = player;
		this.listeNPC = listeNPC;
		this.combatPanel = new CombatPanel(this);
		this.listeSaves = new ArrayList<String>();

		//On initialise la liste des noms de sauvegarde
		File folder = new File("./sauvegardes");
		File[] listOfFiles = folder.listFiles();
		for (File f : listOfFiles) {
			if (f.isFile()) {
				listeSaves.add(f.getName());
			}
		}



	}

	/**
	 * est appele lorsque qu'une touche du clavier est appuyee
	 * @param KeyEvent e : evenement clavier
	 */
	public void keyPressedInDeplacementPanel(KeyEvent e) {
		int key = e.getKeyCode(); 

		//le joueur appuie sur la fleche gauche
		if ((key == KeyEvent.VK_LEFT)) { 

			//On deplace le personnage dans le Modele
			player.deplacergauche(grille);

			//on deplace l'image du joueur sur la Vue
			dp.setXJoueur(player.getPosition().getX()*dp.getTailleCaseEnPixel());

			//On verifie les interactions possibles
			this.interaction();
		}
		else if ((key == KeyEvent.VK_RIGHT)) {
			player.deplacerdroite(grille);
			dp.setXJoueur(player.getPosition().getX()*dp.getTailleCaseEnPixel());
			this.interaction();

		}
		else if ((key == KeyEvent.VK_UP)) {
			player.deplacerhaut(grille);
			dp.setYJoueur(player.getPosition().getY()*dp.getTailleCaseEnPixel());
			this.interaction();

		}
		else if ((key == KeyEvent.VK_DOWN)) {
			player.deplacerbas(grille);
			dp.setYJoueur(player.getPosition().getY()*dp.getTailleCaseEnPixel());
			this.interaction();

		}

		//On verifie si le joueur est sur une case dangereuse
		if (player.getPosition() instanceof Dangereuse) {

			//On test si un combat est lance
			if (((Dangereuse) player.getPosition()).declenche_combat()) {

				// on genere un pokemon sauvage dont le niveau est fixe sur celui du joueur
				Pokemon sauvage = new PokeSauvage(player);

				//On initialise le combat
				this.setCombatVS(new CombatVS(player.getPokemonParDefaut(),sauvage, player, null));

				//On lance le combat
				this.combat();
			}
		}
		//Si le joueur est sur un centre de soins
		else if (player.getPosition() instanceof CentreSoins) {
			//On soigne le joueur
			((Soigneur) player.getPosition().getPersonnage()).soigner(player.getEquipe());
			//et on recharge les pokeballs et potions
			player.getInventaire().add(new Pokeball());
			player.getInventaire().add(new Pokeball());
			player.getInventaire().add(new Pokeball());
			player.getInventaire().add(new Potion1());
			player.getInventaire().add(new Potion1());
			player.getInventaire().add(new Potion1());
		}

		//Si le joueur appuie sur echap, on retourne au menu principal
		if (key == KeyEvent.VK_ESCAPE) {
			this.getFp().getLayout().show(fp.getMainPanel(), "menu");
		}

		//On MAJ la carte
		dp.repaint();

	}


	/**
	 * interagit avec un personnage proche (pour l'instant, seulement des dresseurs)
	 */
	public void interaction() {
		
		// on recupere les coordonnees joueur
		Case cj = player.getPosition();
		int jx = cj.getX();
		int jy = cj.getY();
		for ( Personnage p : listeNPC) {
			//pour chaque NPC, on verifie si le joueur est dans une case horizontale ou verticale avoisinante
			if ( (jx == p.getPosition().getX()+1 && jy == p.getPosition().getY()) || (jx == p.getPosition().getX()-1 && jy == p.getPosition().getY()) || (jx == p.getPosition().getX() && jy == p.getPosition().getY()+1 ) || (jx == p.getPosition().getX() && jy == p.getPosition().getY()-1 )) {
				if (player.interagir(p.getPosition()).equals("Combat")) {
					
					//Si il y a combat, on equilibre le niveau du dresseur avec celui du joueur
					((Dresseur) p).equilibrageNiveau(player);
					this.setCombatVS(new CombatVS(player.getPokemonParDefaut(), ((Dresseur) p).getEquipe().get(0), player, (Dresseur) p));
					this.combat();
				}
			}
		}
	}

	/**
	 * Methode qui fait attaquer l'attaquant, diminuer la vie/Def/Att/Esq de l'attaque et affiche un message puis passe au tour suivant
	 * @param Attaque att
	 * @param Pokemon attaquant
	 * @param Dresseur passif
	 * @param Dresseur dpassif
	 */
	public void attaque(Attaque att, Pokemon attaquant, Pokemon passif, Dresseur dpassif )  {

		int vieAvantAttaque = passif.getVie();
		
		//Le pokemon attaque (normale, buff ou debuff)
		int dgtsEnvoyes = attaquant.attaquer(att);
		//l'attaque re�oit  les degats ponderes selon sa defense et son type
		passif.recoitDegats(att, dgtsEnvoyes);

		//La suite ne fait qu'afficher le message adapte selon l'attaque qui a ete lancee
		//Pour une attaque normale
		if (att instanceof AttPhysique) {
			//l'attaquant peut avoir rate son attaque
			if (dgtsEnvoyes == 0) {
				
				//Creation du message et affichage
				MessagePanel echecAttaque = new MessagePanel(this, attaquant.getNom() +" utilise " + att.getNom() + " mais rate son attaque" );
				combatPanel.addPanel(echecAttaque,(combatPanel.getWidth()-1000)/2, 500, 1000, 50);
				
				//suppression du message apres quelques secondes et passage au tour suivant
				combatPanel.removePanelTourSuivant(echecAttaque); 
			}
			
			//le pokemon ennemi peut esquiver l'attaque
			else if (vieAvantAttaque-passif.getVie() == 0) {
				MessagePanel esquive = new MessagePanel(this, attaquant.getNom() +" utilise " + att.getNom() + " mais " +passif.getNom() +" equive" );
				combatPanel.addPanel(esquive,(combatPanel.getWidth()-1000)/2, 500, 1000, 50);
				combatPanel.removePanelTourSuivant(esquive);
			}
			
			//l'attaque a reussi
			else {
				MessagePanel degatsInfliges = new MessagePanel(this, attaquant.getNom() +" utilise " + att.getNom() + " et inflige " + (vieAvantAttaque-passif.getVie()) + " degats" );
				combatPanel.addPanel(degatsInfliges,(combatPanel.getWidth()-1000)/2, 500, 1000, 50);
				combatPanel.removePanelTourSuivant(degatsInfliges);
				
				//si le pokemon attaque n'a plus de vie, le combat est arrete par defaut
				if (passif.getVie() ==0) {
					combat.setCombatFini(true);
					
					//si le pokemon ennemi a un dresseur (= n'est pas sauvage)
					if (dpassif != null) {
						
						//on cherche un pokemon encore vivant et dans ce cas, le combat continue
						for (Pokemon p : dpassif.getEquipe()) {
							if (p.getVie() !=0) {
								combat.setCombatFini(false);
							}
						}
					}
				}   
			}
		}

		//l'attaque etait un buff (=augmente une stats)
		else if (att.isBuffer()) {
			
			//soit elle augmente l'Attaque
			if (att instanceof Att) {
				MessagePanel buffAtt = new MessagePanel(this, attaquant.getNom() +" utilise " + att.getNom() + " et augmente son attaque !" );
				combatPanel.addPanel(buffAtt,(combatPanel.getWidth()-1000)/2, 500, 1000, 50);
				combatPanel.removePanelTourSuivant(buffAtt);
			}
			//soit elle augmente la Defense
			else if (att instanceof Def) {
				MessagePanel buffDef = new MessagePanel(this, attaquant.getNom() +" utilise " + att.getNom() + " et augmente sa defense !" );
				combatPanel.addPanel(buffDef,(combatPanel.getWidth()-1000)/2, 500, 1000, 50);
				combatPanel.removePanelTourSuivant(buffDef);
			}
			//soit elle augmente l'Esq
			else if (att instanceof Esq) {
				MessagePanel buffEsq = new MessagePanel(this, attaquant.getNom() +" utilise " + att.getNom() + " et augmente son esquive !" );
				combatPanel.addPanel(buffEsq,(combatPanel.getWidth()-1000)/2, 500, 1000, 50);
				combatPanel.removePanelTourSuivant(buffEsq);
			}
		}
		
		//L'attaques etait un debuffer (=diminue une stat adverse)
		else if (!att.isBuffer()) {
			
			//Elle diminue l'attaque ennemie
			if (att instanceof Att) {
				MessagePanel debuffAtt = new MessagePanel(this, attaquant.getNom() +" utilise " + att.getNom() + " et diminue l'attaque de " + passif.getNom());
				combatPanel.addPanel(debuffAtt,(combatPanel.getWidth()-1000)/2, 500, 1000, 50);
				combatPanel.removePanelTourSuivant(debuffAtt);
			}
			//ou sa defense
			else if (att instanceof Def) {
				MessagePanel debuffDef = new MessagePanel(this, attaquant.getNom() +" utilise " + att.getNom() + " et diminue la defense de " + passif.getNom() );
				combatPanel.addPanel(debuffDef,(combatPanel.getWidth()-1000)/2, 500, 1000, 50);
				combatPanel.removePanelTourSuivant(debuffDef);
			}
			//ou son esquive
			else if (att instanceof Esq) {
				MessagePanel debuffEsq = new MessagePanel(this, attaquant.getNom() +" utilise " + att.getNom() + " et diminue l'esquive de " + passif.getNom() );
				combatPanel.addPanel(debuffEsq,(combatPanel.getWidth()-1000)/2, 500, 1000, 50);
				combatPanel.removePanelTourSuivant(debuffEsq);
			}
		}



	}

	/**
	 * Change le pokemon du joueur lors d'un combat
	 * @param String nomPoke
	 */
	public void clicChangement(String nomPoke) {
		//On cherche le pokemon du meme nom dans l'equipe du Joueur
		for (Pokemon p : player.getEquipe()) {
			if (p.getNom().equals(nomPoke)) {
				if (player.getPokemonParDefaut().getVie() == 0) {
					player.setPokemonParDefaut(p);
				}
				this.combat.setCombattant1(p);
				
			}		
		}
	}

	/**
	 * Lance une tentative de fuite du joueur, affiche un message affichant le resultat
	 * et lance le tour suivant ou le retour a la carte selon le resultat
	 */
	public void clicFuite() {
		
		//arrete le combat si la fuite reussit
		combat.setCombatFini(player.fuir(this.combat.getCombattant1().getNiveau(),this.combat.getCombattant2().getNiveau()));

		if (!combat.getCombatFini()) {
			
			//affiche le message
			MessagePanel echecFuite = new MessagePanel(this,"La fuite a echoue");
			combatPanel.addPanel(echecFuite,(combatPanel.getWidth()-300)/2, 500, 300, 	50);
			
			//tour suivant
			combatPanel.removePanelTourSuivant(echecFuite);
		}
		else {
			MessagePanel reussiteFuite = new MessagePanel(this,"La fuite a reussi");
			combat.setCombatFini(false);
			combatPanel.addPanel(reussiteFuite,(fp.getWidth()-300)/2, 500, 300, 50);
			
			//retour a la carte
			combatPanel.removePanelRetourCarte(reussiteFuite);

		}

	}

	/**
	 * Soigne le pokemon du joueur 
	 * @param PotionSoin potion : la potion utilisee
	 */
	public void clicSoin(PotionSoin potion) {
		int vieAvantSoin = combat.getCombattant1().getVie();
		
		//on utilise l'objet
		player.utiliserObjet(potion,this.combat.getCombattant1());
		
		MessagePanel soin = new MessagePanel(this, combat.getCombattant1().getNom() + " recupere " + (combat.getCombattant1().getVie()-vieAvantSoin) + " points de vie");
		combatPanel.addPanel(soin,(combatPanel.getWidth()-1000)/2, 100, 1000, 50);
		combatPanel.removePanelTourSuivant(soin);
	}

	/**
	 * Tente de capturer le pokemon sauvage adverse
	 * @param AttrapeBall a : la pokeball utilisee
	 */
	public void clicCapture(AttrapeBall a) {
		
		if (player.capturer((AttrapeBall) a,this.combat.getCombattant2())) {
			//si la capture reussit, le combat s'arrete
			combat.setCombatFini(true);
			
			MessagePanel captureReussie = new MessagePanel(this, "Le pokemon a ete capture");
			combatPanel.addPanel(captureReussie,(combatPanel.getWidth()-1000)/2, 100, 1000, 50);
			combatPanel.removePanelRetourCarte(captureReussie);
		}
		else {
			MessagePanel echecCapture = new MessagePanel(this, "La capture a echoue");
			combatPanel.addPanel(echecCapture,(combatPanel.getWidth()-1000)/2, 100, 1000, 50);
			combatPanel.removePanelTourSuivant(echecCapture);
		}
	}


	/**
	 * Ajoute Ou retire un Pokemon de la liste de Pokemon du joueur a son equipe de combat
	 * @param BoutonEquipe boutonEquipe : le bouton qui a appele la fonction
	 */
	public void clicChangementEquipe(BoutonEquipe boutonEquipe) {
		
		//on teste si le bouton est un bouton de retrait ou d'ajout
		if (boutonEquipe.getText().equals("retirer de l'equipe")) {
			
			//On ajoute le pokemon a l'inventaire de Pokemon et on le retire de l'equipe
			player.getInventairePokemon().add(boutonEquipe.getPoke());
			player.getEquipe().remove(boutonEquipe.getPoke());

			//on MAJ le panel de l'equipe
			equipe.removeAll();
			equipe.refresh();
			
			//On MAJ le panel de l'inventaire Pokemon
			inventairePokemon.removeAll();
			inventairePokemon.refresh();

		}
		else if (boutonEquipe.getText().equals("ajouter a l'equipe")) {
			
			//On ajoute le pokemon a l'equipe et on le retire de l'inventaire  Pokemon
			player.getEquipe().add(boutonEquipe.getPoke());
			player.getInventairePokemon().remove(boutonEquipe.getPoke());
			
			//On MAJ le panel inventaire Pokemon
			inventairePokemon.removeAll();
			inventairePokemon.refresh();
			
			//On MAJ le panel equipe
			equipe.removeAll();
			equipe.refresh();

		}
	}

	/**
	 * Est appele lorsqu'un clic est effectue sur un bouton
	 * 
	 * @param txt
	 */
	public void clicDetecte(String txt) {	
		//MAJ du panel combat (pour les combats)
		combatPanel.removeAll();
		combatPanel.revalidate();
		combatPanel.repaint();
		this.afficheCombattants();
		
		//On test quel bouton a ete clique
		//On a pu cliquer
		//depuis le menu : sur Jouer, Equipe, Inventaire Pokemon, Sauvegarder, Charger et Quitter
		//depuis le panel "equipe" : Retour Menu, Retirer de l'equipe, attaque d'un pokemon (sans effet)
		//depuis le panel inventairePokemon : Retour Menu, Ajouter a l'equipe, attaque d'un pokemon (sans effet)
		//depuis le panel Combat : Attaquer, Capturer, Soigner son pokemon, Fuir, Changer de Pokemon
		//							ainsi que une attaque, une potion, et une pokeball

		if(txt.equals("Jouer")) {
			
			//On cache le menu et on affiche la carte en demandant le focus pour detecter les entrees claviers
			this.getMp().setVisible(false);
			this.getMp().setFocusable(false);
			this.getDp().setVisible(true);;
			this.getDp().requestFocusInWindow();
			
			//on change de panel via le CardLayout
			this.getFp().getLayout().show(fp.getMainPanel(), "jeu");
		}
		else if (txt.equals("Sauvegarder")) {
			this.getMp().setVisible(false);
			this.getMp().setFocusable(false);
			this.getFp().getLayout().show(fp.getMainPanel(), "sauvegarder");
		}
		else if (txt.equals("Charger")) {
			this.getMp().setVisible(false);
			this.getMp().setFocusable(false);
			this.getSavePanel().setVisible(false);
			this.getSavePanel().setFocusable(false);
			this.getLoadPanel().setVisible(true);
			this.getLoadPanel().setFocusable(true);
			this.getFp().getLayout().show(fp.getMainPanel(), "charger");
		}
		else if (txt.equals("Quitter")) {
			this.getFp().dispose();
		}
		else if (txt.equals("Retour Menu")) {
			this.getEquipe().setVisible(false);
			this.getEquipe().setFocusable(false);
			this.getInventairePokemon().setVisible(false);
			this.getInventairePokemon().setFocusable(false);
			
			this.getMp().setVisible(true);;
			this.getMp().requestFocusInWindow();
			this.getFp().getLayout().show(fp.getMainPanel(), "menu");

		}
		else if (txt.equals("Equipe")) {
			this.getMp().setVisible(false);
			this.getMp().setFocusable(false);
			this.getEquipe().setVisible(true);;
			this.getEquipe().requestFocusInWindow();
			this.getFp().getLayout().show(fp.getMainPanel(), "equipe");
		}
		else if (txt.equals("Inventaire Pokemon")) {
			this.getMp().setVisible(false);
			this.getMp().setFocusable(false);
			this.getInventairePokemon().setVisible(true);;
			this.getInventairePokemon().requestFocusInWindow();
			this.getInventairePokemon().removeAll();
			this.getInventairePokemon().refresh();
			this.getFp().getLayout().show(fp.getMainPanel(), "inventairePokemon");
		}
		else if (txt.equals("Annuler")) {
			combatPanel.removeAll();
			combatPanel.revalidate();
			combatPanel.repaint();
			this.afficheCombattants();
			this.tourCombatJoueur();
		}
		else if (txt.equals("Fuir")) {
			clicFuite();
		}

		
		else if (txt.equals("Attaquer")) {
			
			//Si on choisit d'attaquer, on cree un choixPanel qui presentera les attaques disponibles
			ArrayList<String> listeAtt = new ArrayList<String>();
			for (Attaque att : combat.getCombattant1().getAttaques()) {
				listeAtt.add(att.getNom());
			}
			listeAtt.add("Annuler");
			ChoixPanel choixAttaque = new ChoixPanel(this,listeAtt);
			combatPanel.addPanel(choixAttaque,(combatPanel.getWidth()-1000)/2, 700, 1000, 200);
		}

		else if (txt.equals("soigner son pokemon")) {
			//si on choisit de soigner, on cree un choixpanel presentant les potions disponibles
			ArrayList<String> listePotions = new ArrayList<String>();
			for (Objet o : player.getInventaire()) {
				if (o instanceof PotionSoin) {
					listePotions.add(o.getNom());
				}
			}
			listePotions.add("Annuler");
			
			//si on possede des potions, on affiche le choixPanel
			if (!listePotions.isEmpty()) {
				ChoixPanel choixPotion = new ChoixPanel(this,listePotions);
				combatPanel.addPanel(choixPotion,(combatPanel.getWidth()-1000)/2, 700, 1000, 200);
			}
			//sinon, seulement un message
			else {
				MessagePanel pasDePotion = new MessagePanel(this, "Vous ne possedez pas de potion de soin");
				combatPanel.addPanel(pasDePotion,(combatPanel.getWidth()-1000)/2, 100, 1000, 50);
				combatPanel.removePanel(pasDePotion);

			}
		}

		else if (txt.equals("Changer de Pokemon")) {
			//On choisit de changer de pokemon, on affiche les pokemon disponibles
			ArrayList<String> equipe = new ArrayList<String>();
			for (Pokemon p : player.getEquipe()) {
				//On prend les pokemons non KO
				if (p.getVie()!=0) {
					equipe.add(p.getNom());
				}
			}
			equipe.add("Annuler");
			ChoixPanel changerPokemon = new ChoixPanel(this,equipe);
			combatPanel.addPanel(changerPokemon,(combatPanel.getWidth()-1000)/2, 700, 1000, 200);
		}

		else if (txt.equals("Capturer")) {
			//On choisit de capturer. On recupere les pokeballs disponibles
			ArrayList<String> liste = new ArrayList<String>();
			for (Objet o : player.getInventaire()) {
				if (o instanceof AttrapeBall) {
					liste.add(o.getNom());
				}
			}
			liste.add("Annuler");
		
			if (!liste.isEmpty()) {
				//Il reste des pokeballs disponibles.On les affiche
				ChoixPanel choixPokeball = new ChoixPanel(this,liste);
				combatPanel.addPanel(choixPokeball,(combatPanel.getWidth()-1000)/2, 600, 1000, 200);
			}
			else {
				//plus de pokeballs disponibles
				MessagePanel pasDePokeball = new MessagePanel(this, "Vous ne possedez pas de pokeball");
				combatPanel.addPanel(pasDePokeball,(combatPanel.getWidth()-1000)/2, 100, 1000, 50);
				combatPanel.removePanel(pasDePokeball);
			}
		}

		else {		
			
			//Cas o� apres avoir choisi de changer de  pokemon, on a clique sur un des pokemons disponibles
			for (Pokemon p : player.getEquipe()) {
				
				//On cherche le pokemon choisi en comparant les noms
				if (txt.equals(p.getNom())) {
					
					//On change le pokemon via la methode clicChangement
					clicChangement(txt);
					
					//affichage d'un msg et MAJ du panel
					MessagePanel changementReussi = new MessagePanel(this, "Le nouveau combattant est " + combat.getCombattant1().getNom());
					combatPanel.removeAll();
					this.afficheCombattants();
					combatPanel.revalidate();
					combatPanel.repaint();
					combatPanel.addPanel(changementReussi,(combatPanel.getWidth()-1000)/2, 100, 1000, 50);
					
					//tour suivant
					combatPanel.removePanelTourSuivant(changementReussi); 
					break;
				}

			}

			//Cas o� apres avoir choisi de se soigner ou de capturer le pokemon adverse
			for (Objet o : player.getInventaire()) {
				
				//On cherche la pokeball choisie dans l'inventaire et on appelle clicCapture
				if (o instanceof AttrapeBall && txt.equals(o.getNom())) {
					clicCapture((AttrapeBall) o);
					break;
				}
				//ou la potion choisie et on appelle clicSoin
				else if (o instanceof PotionSoin && txt.equals(o.getNom())){
					clicSoin((PotionSoin) o);
					break;
				}
			}
			
			//Cas o� apres avoir choisi d'attaquer, on a clique sur une des attaques proposees
			for (Attaque a : combat.getCombattant1().getAttaques()) {
				if (txt.equals(a.getNom())) {
					attaque(a, combat.getCombattant1(), combat.getCombattant2(), combat.getProprietaire2());
					break;
				}
			}



		}
	}


	/**
	 * Lance le tour du joueur lros d'un combat
	 */
	public void tourCombatJoueur() {

		//On liste les actions possibles
		ArrayList<String> actions = new ArrayList<String>();
		
		
		actions.add("Changer de Pokemon");
		//Si le pokemon n'a plus de vie, on n'a pas d'autres options que de changer de Pokemon
		if (combat.getCombattant1().getVie() !=0) {
			actions.add("Attaquer");
			actions.add("Fuir");
			actions.add("soigner son pokemon");
			
			//Si le pokemon est sauvage, on peut de plus le capturer
			if (combat.getCombattant2() instanceof PokeSauvage) {
				actions.add("Capturer");
			}
		}
		else {
			MessagePanel changementObligatoire = new MessagePanel(this, "Vous devez changer de pokemon");
			combatPanel.addPanel(changementObligatoire, (combatPanel.getWidth()-1000)/2, 300, 1000, 50);
		}
		
		//affichage des options disponibles
		ChoixPanel choix = new ChoixPanel(this,actions);
		combatPanel.addPanel(choix,(combatPanel.getWidth()-1000)/2, 800, 1000, 200);
	}

	
	/**
	 * lance le tour de l'IA lors d'un combat
	 */
	public void tourIA() {
		Random r = new Random();
		
		//Si le pokemon est KO
		if (combat.getCombattant2().getVie() ==0) {
			
			//on cherche un pokemon en vie dans l'equipe du 2eme dresseur
			for (Pokemon p : combat.getProprietaire2().getEquipe()) {
				if (p.getVie() !=0) { 
					
					//et on actualise le combattant2
					combat.setCombattant2(p);
					
					combatPanel.removeAll();
					combatPanel.revalidate();
					combatPanel.repaint();
					this.afficheCombattants();
					MessagePanel changementObligatoire = new MessagePanel(this, combat.getProprietaire2().getNom() + " change de pokemon");
					combatPanel.addPanel(changementObligatoire, (combatPanel.getWidth()-200)/2, 300, 300, 50);
					combatPanel.removePanelTourSuivant(changementObligatoire);
					break;
				}
			}
		}
		
		//le pokemon a encore de la vie
		else {
			
			//on prend aleatoirement une des attaques du pokemon IA
			int alea = r.nextInt(combat.getCombattant2().getAttaques().size());
			this.attaque(combat.getCombattant2().getAttaques().get(alea),combat.getCombattant2(),combat.getCombattant1(),combat.getProprietaire1());
		}
	}

	/**
	 * Determine selon le numero du tour qui doit jouer
	 */
	public void tourCombat() {
		
		
		if (combat.getTour() % 2 != 0) {
			//C'est le tour du joueur
			
			//si le pokemon est KO, affichage d'un message
			if (combat.getCombattant1().getVie() == 0) {
				MessagePanel ko = new MessagePanel(this,combat.getCombattant1().getNom()+" est K.O.");
				combatPanel.addPanel(ko,(combatPanel.getWidth()-1000)/2, 400, 1000, 50);
				combatPanel.removePanel(ko);
			}
			
			//Si le combat est fini (tous les pokemon sont KO) lors du tour du joueur, c'est qu'il a perdu
			if (combat.getCombatFini()) {
				MessagePanel defaite = new MessagePanel(this,"Vous avez perdu et vous rendez a votre base");
				combatPanel.addPanel(defaite,(combatPanel.getWidth()-1000)/2, 650, 1000, 50);
				
				//suppression du messagepanel et retout a la carte
				combatPanel.removePanelRetourCarte(defaite);
				
				//Le joueur est ramene a un point arbitraite (centre de soin ici)
				player.setPosition(grille.getCases().get(105).get(99));
				//Il est soigne
				((Soigneur) player.getPosition().getPersonnage()).soigner(player.getEquipe());
				
				//MAJ de la vue
				dp.setYJoueur(player.getPosition().getY()*dp.getTailleCaseEnPixel());
				dp.setXJoueur(player.getPosition().getX()*dp.getTailleCaseEnPixel());
				dp.revalidate();
				dp.repaint();
			}
			else {
				//le combat n'est pas fini
				combat.setTour(combat.getTour()+1);
				
				//c'est donc au tour du joueur d'agir
				tourCombatJoueur();

			}

		}
		
		//Sinon, c'est le tour de l'IA
		else {

			//Si le pokemon IA est KO, affichage d'un message
			if (combat.getCombattant2().getVie() == 0) {
				MessagePanel ko = new MessagePanel(this,combat.getCombattant2().getNom()+" est K.O.");
				combatPanel.addPanel(ko,(combatPanel.getWidth()-1000)/2, 400, 1000, 50);
				combatPanel.removePanel(ko);
			}
			
			//Si le combat est fini alors que c'est le tour de l'IA, victoire du joueur
			if (combat.getCombatFini()) {
				MessagePanel victoire = new MessagePanel(this,"Vous avez vaincu");
				combatPanel.addPanel(victoire,(combatPanel.getWidth()-1000)/2, 650, 1000, 50);	
				
				//Si l'ennemi est un dresseur, on reinitialise ses pokemon
				if (combat.getProprietaire2() != null) {
					ArrayList<Pokemon> liste = new ArrayList<Pokemon>();
					for (int i = 0; i < 6; i++) {
						// Confere au dresseur 6 pokemons tires aleatoirement
						Pokemon poke = new PokeDresse();
						if (liste.contains(poke) == false) {
							liste.add(poke);
							i++;
						}
					}
					combat.getProprietaire2().setEquipe(liste);
					
				}
				//Si on a battu un pokemon de niveau superieur au notre
				if (combat.getCombattant2().getNiveau() > combat.getCombattant1().getNiveau()) {
					
					//gain de niveau
					combat.getCombattant1().gainXP();
					
					MessagePanel gainNiveau = new MessagePanel(this,combat.getCombattant1().getNom() + " gagne un niveau !");
					combatPanel.addPanel(gainNiveau,(combatPanel.getWidth()-1000)/2, 700, 1000, 50);
					combatPanel.removePanel(gainNiveau);
					
					//Si notre pokemon n'est pas totalement evolue et est maintenant de niveau suffisant poour evoluer
					if (combat.getCombattant1().getEvolutionSuivante()!=null && combat.getCombattant1().getNiveau() == combat.getCombattant1().getEvolutionSuivante().getNiveau()) {

						//Il evolue
						combat.getCombattant1().evolution();
						
						MessagePanel evolution = new MessagePanel(this,combat.getCombattant1().getNom()+ " evolue en " +combat.getCombattant1().getEvolutionSuivante().getNom());
						combatPanel.addPanel(evolution,(combatPanel.getWidth()-1000)/2, 800, 1000, 50);
						combatPanel.removePanel(evolution);
					}
				}
				
				//Retour a la carte
				combatPanel.removePanelRetourCarte(victoire);
			}
			else {
				//Sinon, le combat n'est pas fini, c'est au tour de l'IA
				combat.setTour( combat.getTour()+1);
				tourIA();
			}
		}
	}

	/**
	 * Initialise le combat
	 */
	public void combat() {
		
		//affichage et MAJ du panel de combat
		combatPanel.removeAll();
		combatPanel.revalidate();
		combatPanel.repaint();
		this.afficheCombattants();
		fp.getLayout().show(fp.getMainPanel(), "combat");

		//Si le pokemon adverse est sauvage, un message apparait
		if (combat.getCombattant2() instanceof PokeSauvage) {
			MessagePanel intro = new MessagePanel(this,"Un " + combat.getCombattant2().getNom() + " sauvage apparait");
			combatPanel.addPanel(intro,(combatPanel.getWidth()-450)/2, 600, 450, 100);//combatPanel.getWidth()-1000)/2, 200, 100, 25);
			combatPanel.removePanel(intro);
		}
		//sinon c'est un dresseur -> on affiche un message different
		else {
			MessagePanel intro = new MessagePanel(this,combat.getProprietaire2().getNom() + " vous defie !");
			combatPanel.addPanel(intro,(combatPanel.getWidth()-400)/2, 600, 400, 100);//combatPanel.getWidth()-1000)/2, 200, 100, 25);
			combatPanel.removePanel(intro);
		}

		//Apres 1 seconde (le temps de lire le message) on lance le combat
		Timer timer = new Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				tourCombat();
			}

		});
		timer.setRepeats(false);
		timer.start();

	}

	/**
	 * Cree et ajoute les panels representant les 2 combattants
	 */
	public void afficheCombattants() {
		
		//le combattant 1 : le 0 signifie qu'il est a gauche, pour l'orientation de l'image
		PokemonPanel combattant1 = new PokemonPanel(this,combat.getCombattant1(), 0);
		combatPanel.addPanel(combattant1,150, 200, 400, 300);
		
		//le combattant 2 : le 1 signifie qu'il est a droite, pour l'orientation de l'image
		PokemonPanel combattant2 = new PokemonPanel(this,combat.getCombattant2(),1);
		combatPanel.addPanel(combattant2,combatPanel.getWidth()-550,200, 400, 300);
	}


	public void sauvegarde(String savename) {
		//String filePath = "/home/formation/eclipse-workspace/ProjetPokemon/sauvegarde.txt";
		if (savename.equals("Nouvelle Sauvegarde")) {
			if (!listeSaves.isEmpty()) {
				savename = listeSaves.get(listeSaves.size()-1);
				savename = savename.substring(0, savename.length()-5) + (Integer.parseInt(savename.substring(10, savename.length()-4))+1);

			}
			else {
				savename = "sauvegarde1";
			}
			this.listeSaves.add(savename+".txt");
		}
		else {
			savename = savename.substring(0, savename.length()-4);
		}

		String filePath = "sauvegardes/"+savename+".txt";
		Path logFile = Paths.get(filePath);
		if (!Files.exists(logFile)) {
			try {
				Files.createFile(logFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try (BufferedWriter writer = Files.newBufferedWriter(logFile,  StandardCharsets.UTF_8,StandardOpenOption.WRITE)) {
			writer.write(player.getNom()+"\n");
			writer.write(player.getPosition().getX()+ " " + player.getPosition().getY()+"\n");
			for (Pokemon p : player.getEquipe()) {
				writer.write(p.getNom()+" "+ p.getNiveau()+" "+ p.getVie()+"\n");
			}
			writer.write("---\n");
			for (Pokemon p : player.getInventairePokemon()) {
				writer.write(p.getNom()+" "+ p.getNiveau()+" "+ p.getVie()+"\n");
			}
			writer.write("---\n");
			for (Objet o : player.getInventaire()) {
				writer.write(o.getClass().toString().split("\\.")[1] + " " + o.getNom()+"\n");
			}
			writer.write("END");

		}
		catch (IOException e) {
			e.printStackTrace();
		}

/*		this.getSavePanel().refresh();
		this.getSavePanel().revalidate();
		this.getSavePanel().repaint();
*/
		this.getSavePanel().setVisible(false);
		this.getSavePanel().setFocusable(false);
		this.getMp().setVisible(true);
		this.getMp().requestFocusInWindow();
		this.getFp().getLayout().show(fp.getMainPanel(), "menu");
	}

	public void chargeSauvegarde(String savename) {
		this.getLoadPanel().refresh();
		this.getLoadPanel().revalidate();
		String filePath = "sauvegardes/"+savename;// "/home/formation/eclipse-workspace/ProjetPokemon/sauvegarde.txt";
		//this.getLoadPanel().refresh();
		Path p = Paths.get(filePath);;
		try (BufferedReader reader = Files.newBufferedReader(p)) {
			String line;
			String[] splitline;
			line = reader.readLine();
			player.setNom(line);
			splitline = reader.readLine().split("\\ ");
			player.setPosition(grille.getCases().get(Integer.parseInt(splitline[0])).get(Integer.parseInt(splitline[1])));
			dp.setXJoueur(player.getPosition().getX()*dp.getTailleCaseEnPixel());
			dp.setYJoueur(player.getPosition().getY()*dp.getTailleCaseEnPixel());

			player.setEquipe(new ArrayList<Pokemon>());
			player.setInventairePokemon(new ArrayList<Pokemon>());
			player.setInventaire(new ArrayList<Objet>());

			while (!(line = reader.readLine()).equals( "---")) {
				splitline = line.split("\\ ");
				PokeDresse poke = new PokeDresse(splitline[0],Integer.parseInt(splitline[1]));
				player.getEquipe().add(poke);
				poke.setVie(Integer.parseInt(splitline[2]));
			}
			while (!(line = reader.readLine()).equals( "---")) {
				splitline = line.split("\\ ");
				PokeDresse poke = new PokeDresse(splitline[0],Integer.parseInt(splitline[1]));
				player.getInventairePokemon().add(poke);
				poke.setVie(Integer.parseInt(splitline[2]));
			}
			while (!(line = reader.readLine()).equals( "END")) {
				splitline = line.split("\\ ");
				if (splitline[0].equals("Potion1")) {
					player.getInventaire().add(new Potion1());
				}
				else if (splitline[0].equals("Pokeball")) {
					player.getInventaire().add(new Pokeball());
				}
				else if (splitline[0].equals("Superball")) {
					player.getInventaire().add(new Superball());
				}
				else if (splitline[0].equals("Megaball")) {
					player.getInventaire().add(new Megaball());
				}
				else if (splitline[0].equals("Gigaball")) {
					player.getInventaire().add(new Gigaball());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		dp.revalidate();
		dp.repaint();
		
		this.getLoadPanel().revalidate();
		this.getLoadPanel().setVisible(false);
		this.getLoadPanel().setFocusable(false);
		this.getDp().setVisible(true);;
		this.getDp().requestFocusInWindow();
		this.getFp().getLayout().show(fp.getMainPanel(), "jeu");
	}



	public void setCombatVS(CombatVS combat) {
		this.combat = combat;
	}

	public void setCombatPanel(CombatPanel combatPanel) {
		this.combatPanel = combatPanel;

	}

	public FenetrePrincipale getFp() {
		return fp;
	}

	public void setFp(FenetrePrincipale fp) {
		this.fp = fp;
	}

	public CombatPanel getCombatPanel() {
		return combatPanel;
	}

	public void setDp(DeplacementPanel dp) {
		this.dp = dp;
	}

	public DeplacementPanel getDp() {
		return dp;
	}

	public MenuPrincipalPanel getMp() {
		return mp;
	}

	public void setMp(MenuPrincipalPanel mp) {
		this.mp = mp;
	}

	public Joueur getPlayer() {
		return player;
	}

	public void setPlayer(Joueur player) {
		this.player = player;
	}

	public EquipePanel getEquipe() {
		return equipe;
	}

	public void setEquipe(EquipePanel equipe) {
		this.equipe = equipe;
	}

	public ArrayList<Personnage> getListeNPC() {
		return listeNPC;
	}

	public void setListeNPC(ArrayList<Personnage> listeNPC) {
		this.listeNPC = listeNPC;
	}

	public InventairePokemonPanel getInventairePokemon() {
		return inventairePokemon;
	}

	public void setInventairePokemon(InventairePokemonPanel inventairePokemon) {
		this.inventairePokemon = inventairePokemon;
	}

	public ArrayList<String> getListeSaves() {
		return listeSaves;
	}

	public void setListeSaves(ArrayList<String> listeSaves) {
		this.listeSaves = listeSaves;
	}

	public LoadAndSavePanel getSavePanel() {
		return savePanel;
	}

	public void setSavePanel(LoadAndSavePanel savePanel) {
		this.savePanel = savePanel;
	}

	public LoadAndSavePanel getLoadPanel() {
		return loadPanel;
	}

	public void setLoadPanel(LoadAndSavePanel loadPanel) {
		this.loadPanel = loadPanel;
	}

	public Grille getGrille() {
		return grille;
	}


}
