package deplacement;

import perso.Personnage;

/**
 * Creation des cases praticables de la grille, ou le joueur pourra se deplacer dessus sans rique de rencontrer un adversaire.
 * @author 
 *
 */
public class Sure extends Case{

	public Sure(int x, int y, Personnage personnage) {
		super(x, y, personnage);
	}
	
	public Sure(int x, int y) {
		super(x, y);
	}

}
