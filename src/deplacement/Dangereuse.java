package deplacement;

/**
 * Creation des cases praticables dangereuses de la grille. Le joueur pourra se d�placer dessus mais risquera de rencontrer un adversaire.
 * @author  
 *
 */

public class Dangereuse extends Case{
	

	private int probacombat;

	public int getProbacombat() {
		return probacombat;
	}
	public void setProbacombat(int probacombat) {
		this.probacombat = probacombat;
	}
	
	public Dangereuse(int x, int y) {
		super(x, y);
		this.probacombat = 15;
	}
	
	/**
	 * Cette methode attribue de facon aleatoire la presence ou non d'un pokemon a combattre sur une case dangereuse.
	 * @return true: presence d'un pokemon; false :case sure sans danger
	 * 
	 */
	public boolean declenche_combat() {
		double d = Math.random();
		
		return (d < ((float) this.probacombat)/((float)100));
	}

}