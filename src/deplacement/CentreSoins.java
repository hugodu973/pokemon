package deplacement;

import perso.Soigneur;
/**
 * Case sure centre soins. Un soigneur se trouve dessus pour soigner toute l'equipe du joueur.
 * @author 
 *
 */
public class CentreSoins extends Sure{

	public CentreSoins(int x, int y) {
		super(x, y, new Soigneur(0,"Maitre Soigneur",null));//new Sure(x,y)));
		
	}

}
