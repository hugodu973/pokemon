package deplacement;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
/**
 * Creation de la grille de jeu, qui contient l'ensemble des cases
 * @author  
 *
 */
public class Grille {
	
	private int longueur;
	private int largeur;
	private ArrayList<ArrayList<Case>> cases;
	
	/**
	 * Constructeur de la grille par defaut, a partir de la couleur des pixels de l'image
	 */
	public Grille() {
		this.longueur = 200;
		this.largeur = 200;
		this.cases = new ArrayList<ArrayList<Case>>();
		for (int i = 0; i < longueur; i++) {
			cases.add(new ArrayList<Case>());
		}
		//recuperation de l'echantillon couleur des pixels constituant la carte
		File input0 = new File("DONNEES//echantillon.png");
		//recuperation de l'image de la carte 
		File input = new File("DONNEES//cartev7.png");
        try {
        	BufferedImage reference = ImageIO.read(input0);
        	
        	//attribution de la valeur RGB a chaque pixel de l'echantillon
        	int dangerVF = reference.getRGB(0,0);
        	int herbeVC = reference.getRGB(1,0);
        	int pierreG = reference.getRGB(2,0);
        	int boisMC = reference.getRGB(3,0);
        	int soinsMF = reference.getRGB(4,0);
        	int eauB = reference.getRGB(5,0);
        	int murO = reference.getRGB(6,0);
        	int plancherJ = reference.getRGB(7,0);
        	int routeB = reference.getRGB(8,0);
        	
       
        	//attribution de la classe de la case en fonction de la couleur de la case, en comparant avec les couleurs references de l'echantillon
			BufferedImage image = ImageIO.read(input);
			for (int i = 0; i < image.getWidth() ; i++) {
				for (int j = 0; j < image.getHeight(); j++) { 
					int p = image.getRGB(i,j);
					
					if (p == dangerVF) {
						this.cases.get(i).add(new Dangereuse(i,j));
					}
					
					if (p == herbeVC) {
						this.cases.get(i).add(new Herbe(i,j));
					}
					
					if (p==pierreG) {
						this.cases.get(i).add(new Pierre(i,j));
					}
					if (p==boisMC) {
						this.cases.get(i).add(new Bois(i,j));
					}
					if(p==soinsMF) {
						this.cases.get(i).add(new CentreSoins(i,j));
					}
					if (p==eauB) {
						this.cases.get(i).add(new Eau(i,j));
					}
					if (p==murO) {
						this.cases.get(i).add(new Mur(i,j));
					}
					if (p==plancherJ) {
						this.cases.get(i).add(new Plancher(i,j));
					}
					if (p==routeB) {
						this.cases.get(i).add(new Route(i,j));
					}
											
				}}

			}
        catch (IOException e) {
			e.printStackTrace();
        }
	}


	
	public ArrayList<ArrayList<Case>> getCases() {
		return cases;
	}

	public void setCases(ArrayList<ArrayList<Case>> cases) {
		this.cases = cases;
	}

	public int getLongueur() {
		return longueur;
	}
	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}
	public int getLargeur() {
		return largeur;
	}
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	
	
}