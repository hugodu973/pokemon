package deplacement;

import perso.Personnage;
/**
 * Creation des differents types de case de la grille
 * @author 
 *
 */
public abstract class Case {
	
	private int x;
	private int y;
	private Personnage personnage;
	
	/**
	 * Constructeur de case sans personnage
	 * @param x ligne
	 * @param y colonne
	 */
	public Case(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Constructeur de case avec personnage
	 * @param x ligne
	 * @param y colonne
	 */
	public Case(int x, int y, Personnage personnage) {
		super();
		this.x = x;
		this.y = y;
		this.personnage = personnage;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Personnage getPersonnage() {
		return personnage;
	}
	public void setPersonnage(Personnage personnage) {
		this.personnage = personnage;
	}
	
	
	
	

}