package deplacement;

import perso.Personnage;
/**
 * Cr�ation des cases impraticables de la grille, o� le joueur ne pourra pas se d�placer dessus. Ces cases correspondent � des obstacles.
 * @author formation
 *
 */
public class Impraticable extends Case{

	public Impraticable(int x, int y, Personnage personnage) {
		super(x, y, personnage);
	}
	public Impraticable(int x, int y) {
		super(x, y);
	}
}
