package InterfaceGraphique;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import controleur.Controleur;
/**
 * Classe permettant la creation de l'interface de combat
 * @author 
 */
public class CombatPanel extends JPanel {
	private Controleur controleur;
	
	/**
	 * Constructeur 
	 * @param Controleur control
	 */
	public CombatPanel(Controleur control) {
		this.controleur= control;
		this.setLayout(null);
	}

	public Controleur getControleur() {
		return controleur;
	}

	public void setControleur(Controleur controleur) {
		this.controleur = controleur;
	}
	
	public void addPanel(Component comp, int x, int y, int width,int height) {
		super.add(comp);
		comp.setBounds(x,y,width,height);
		comp.setVisible(true);
		this.refresh();
	}
	
	public void refresh() {
		this.revalidate();
		this.repaint();
	}
	
	/**
	 * Methode permettant de supprimer le panel en argument du combat panel apres 3 secondes
	 * @param comp
	 */
	public void removePanel(Component comp) {
		CombatPanel temp = this;
		Timer timer = new Timer(3000, new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				temp.remove(comp);
				temp.refresh();
			}
		});
		timer.setRepeats(false);
		timer.start();
	}
	
	/**
	 * Methode permettant de supprimer le panel en argument du combat panel apres 3 secondes pui passer au tour suivant
	 * @param COmponent comp
	 */
	public void removePanelTourSuivant(Component comp) {
		CombatPanel temp = this;
		Timer timer = new Timer(3000, new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				temp.remove(comp);
				temp.refresh();
				temp.getControleur().tourCombat();
				
			}

		});
		timer.setRepeats(false);
		timer.start();
	}
	
	/**
	 * Methode permettant  de supprimer le panel en argument du combat panel apres 3 secondes et de quitter l'interface de combat pour retourner sur la carte
	 * @param Component comp
	 */
	public void removePanelRetourCarte(Component comp) {
		CombatPanel temp = this;
		 Timer timer = new Timer(3000, new ActionListener() {
			 public void actionPerformed(ActionEvent evt) {
				 temp.remove(comp);
				 temp.revalidate();
				 temp.repaint();
				 temp.getControleur().getCombatPanel().setVisible(false);
				 temp.getControleur().getCombatPanel().setFocusable(false);
				 temp.getControleur().getDp().setVisible(true);
				 temp.getControleur().getDp().requestFocusInWindow();
				 temp.getControleur().getFp().getLayout().show(temp.getControleur().getFp().getMainPanel(), "jeu");
			 }

		 });
		 timer.setRepeats(false);
		 timer.start();
	}
}
