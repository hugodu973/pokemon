package InterfaceGraphique;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JLabel;
import javax.swing.JPanel;

import controleur.Controleur;
/**
 * Classe permettant l'affichage d'un message dans l'interface graphique
 * @author
 *
 */
public class MessagePanel extends JPanel {
	private Controleur controleur;
	private String message;

    /**
     * Constructeur pour l'affichage d'un message
     * @param Controleur controleur
     * @param String message
     */
	public MessagePanel(Controleur controleur, String message) {
		this.controleur = controleur;
		this.message = message;
		
		//ajout du texte dans l'interface graphique
		JLabel msg = new JLabel(message);
		msg.setFont(new Font("Serif",Font.PLAIN,25));;
		
		
		//msg.setOpaque(true);
		//msg.setBackground(Color.white);
		//this.setBackground(Color.black);
		//this.setOpaque(true);
		this.add(msg);//, BorderLayout.SOUTH);
		
	}
	
}