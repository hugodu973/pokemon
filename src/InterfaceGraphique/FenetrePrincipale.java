package InterfaceGraphique;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Combat.CombatVS;
import controleur.Controleur;
import pokemon.PokeSauvage;
/**
 *Classe permettant de construire et associer les divers interfaces du jeu: carte, inventaires, equipe...
 */
public class FenetrePrincipale extends JFrame {
		private Controleur controleur;
		private JPanel mainPanel;
		private CardLayout layout;
		
		/**
		 * Constructeur de la fenetre principale
		 * @param Controleur control
		 */
		public FenetrePrincipale(Controleur control) {
			this.controleur = control;
			this.init(control);
			
		}
		
        /**
         * Initialisation de la grille
         * @param Controleur control
         */
		private void init(Controleur control) {
			
			//parametres principaux: titre, taille, disposition
			this.setTitle("Titre");
			this.setSize(1800,1000);
			this.setLocationRelativeTo(null);
			this.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE);
			this.setVisible(true);
			this.layout = new CardLayout();
			
			mainPanel = new JPanel();
			mainPanel.setBackground(Color.cyan);
			mainPanel.setLayout(layout);
			
			//creation de la carte ou le joueur pourra se deplacer pour rencontrer des pokemons
			DeplacementPanel jeu = new DeplacementPanel(control,control.getGrille());
			jeu.setBackground(Color.green);
			this.add(mainPanel,BorderLayout.CENTER);
			
			//creation du menu principal
			MenuPrincipalPanel mp = new MenuPrincipalPanel(control);
			
			//creation de l'equipe de pokemons du joueur
			EquipePanel equipe = new EquipePanel(control,control.getPlayer());
			
			//creation de l'inventaire de pokemons
			InventairePokemonPanel inventairePokemon = new InventairePokemonPanel(control,control.getPlayer());
			
			//sauvegarde
			LoadAndSavePanel savePanel = new LoadAndSavePanel(control,control.getListeSaves(),0);
			LoadAndSavePanel loadPanel = new LoadAndSavePanel(control,control.getListeSaves(),1);
			
			//execution du controleur en lui attribuant les parametres precedement crees
			control.setDp(jeu);
			control.setFp(this);
			control.setMp(mp);
			control.setEquipe(equipe);
			control.setInventairePokemon(inventairePokemon);
			control.setLoadPanel(loadPanel);
			control.setSavePanel(savePanel);
			
			
			//initialisation d'un combat vide
			control.setCombatVS(new CombatVS(control.getPlayer().getPokemonParDefaut(),new PokeSauvage(control.getPlayer()), control.getPlayer(), null));

			
			//creation du menu de combat
			CombatPanel menuCombat = control.getCombatPanel();
			
			//ajout a la fenetre principale mainPanel des interfaces crees
			mainPanel.add(mp,"menu");
			mainPanel.add(equipe,"equipe");
		
			mainPanel.add(jeu,"jeu");
			
			mainPanel.add(menuCombat,"combat");
			mainPanel.add(inventairePokemon,"inventairePokemon");
			mainPanel.add(loadPanel,"charger");
			mainPanel.add(savePanel,"sauvegarder");

			mp.setVisible(true);
			mp.requestFocusInWindow();
			this.revalidate();
			this.repaint();

			
		}

		public Controleur getControleur() {
			return controleur;
		}

		public void setControleur(Controleur controleur) {
			this.controleur = controleur;
		}

		public JPanel getMainPanel() {
			return mainPanel;
		}

		public void setMainPanel(JPanel mainPanel) {
			this.mainPanel = mainPanel;
		}

		public CardLayout getLayout() {
			return layout;
		}

		public void setLayout(CardLayout layout) {
			this.layout = layout;
		}
		
}
