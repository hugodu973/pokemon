package InterfaceGraphique;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import controleur.Controleur;

public class LoadAndSavePanel extends JPanel {
	private Controleur controleur;
	private ArrayList<String> nomsSauvegardes;
	private int SaveOrLoad;
	//0 : sauvegarder
	//1 : charger
	
	public LoadAndSavePanel(Controleur controleur, ArrayList<String> nomsSauvegardes, int SaveOrLoad) {
		this.controleur = controleur;
		this.nomsSauvegardes = nomsSauvegardes;
		//this.setBackground(Color.black);
		this.setLayout(new FlowLayout());
		for (String s : nomsSauvegardes) {
			//String[] nom = o.getClass().toString().split("\\.");
			BoutonLoadAndSave temp = new BoutonLoadAndSave(controleur,s, SaveOrLoad);
			
			this.add(temp);
			//temp.setBounds((1+(len-1)%4) *300, (4+(len-1)%4) *150, 100,30);
			temp.setPreferredSize(new Dimension(Math.max(150,s.length()*10),45));
			
		}
		if (SaveOrLoad == 0) {
			BoutonLoadAndSave temp = new BoutonLoadAndSave(controleur,"Nouvelle Sauvegarde", SaveOrLoad);
			this.add(temp);
		}
	}
	
	public void refresh() {
		this.removeAll();
		for (String s : nomsSauvegardes) {
			//String[] nom = o.getClass().toString().split("\\.");
			BoutonLoadAndSave temp = new BoutonLoadAndSave(controleur,s, SaveOrLoad);
			
			this.add(temp);
			//temp.setBounds((1+(len-1)%4) *300, (4+(len-1)%4) *150, 100,30);
			temp.setPreferredSize(new Dimension(Math.max(150,s.length()*10),45));
			
		}
		if (SaveOrLoad == 0) {
			BoutonLoadAndSave temp = new BoutonLoadAndSave(controleur,"Nouvelle Sauvegarde", SaveOrLoad);
			this.add(temp);
		}
	}
	
}
