package InterfaceGraphique;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import controleur.Controleur;
import pokemon.Pokemon;
/**
 * Classe permettant la cr�ation des boutons pour ajouter ou retirer un pokemon de l'equipe du joueur
 * @author 
 *
 */
public class BoutonEquipe extends JButton implements ActionListener{ // implementation de l'interface ActionListener
		private Controleur controleur;
		private Pokemon poke;
		
		/**
		 * Constructeur du bonton pour gerer l'equipe du joueur
		 * @param Controleur controleur
		 * @param String nom
		 * @param Pokemon poke: pokemon a ajouter ou supprimer
		 */
		public BoutonEquipe(Controleur controleur,String nom, Pokemon poke){
			super(nom);
			this.setPreferredSize(new Dimension(180,150));
			this.addActionListener(this); // on declare que le listener est l'objet lui-meme
			this.controleur = controleur;
			this.poke = poke;
		}
        
		/**
		 * M�thode permettant d'executer l'action du bouton: ajouter ou retirer le pokemon
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			controleur.clicChangementEquipe(this);

			
		}

		public Pokemon getPoke() {
			return poke;
		}

		public void setPoke(Pokemon poke) {
			this.poke = poke;
		}
		
		
	}

