package InterfaceGraphique;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import controleur.Controleur;
/**
 * Classe permettant la cr�ation des divers boutons de l'interface graphique
 * @author 
 *
 */
public class Bouton extends JButton implements ActionListener{ // implementation de l'interface ActionListener
	private Controleur controleur;
	
	/**
	 * Constructeur du bouton
	 * @param Controleur controleur
	 * @param String nom
	 */
	public Bouton(Controleur controleur,String nom){
		super(nom);
		this.setPreferredSize(new Dimension(100,30));
		this.addActionListener(this); // on declare que le listener est l'objet lui-meme
		this.controleur = controleur;
	}
    
	/**
	 * M�thode permettant d'executer l'action du bouton : jouer, retour menu, etc
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//recuperation du texte du bouton pour savoir quelle action effectuer
		controleur.clicDetecte(this.getText());		
	}
}