package InterfaceGraphique;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import controleur.Controleur;
import deplacement.Bois;
import deplacement.Case;
import deplacement.CentreSoins;
import deplacement.Dangereuse;
import deplacement.Eau;
import deplacement.Grille;
import deplacement.Herbe;
import deplacement.Impraticable;
import deplacement.Mur;
import deplacement.Pierre;
import deplacement.Plancher;
import deplacement.Route;
import deplacement.Sure;
import perso.Dresseur;
import perso.Personnage;
/**
 * Classe permettant la construction de l'interface graphique de la grille de jeu case par case 
 * @author formation
 */
public class DeplacementPanel extends JPanel implements KeyListener{
	private int xJoueur, yJoueur;
	private Controleur controleur;
	private int tailleCaseEnPixel;
	private JLayeredPane layout;
	private Grille carte;


	/**
	 * Constructeur du JPanel de la carte de jeu
	 * @param Controleur controleur
	 */
	public DeplacementPanel(Controleur controleur, 	Grille carte) {
		this.tailleCaseEnPixel = 20;
		this.setBackground(Color.black);
		this.setFocusable(true);        // sinon par defaut le panel n�a pas le focus : on ne peut pas interagir avec
		this.addKeyListener( this);      // on declare que this ecoute les evenements clavier
		this.controleur = controleur;

		//apparition du joueur au centre de la grille
		this.xJoueur = 100*tailleCaseEnPixel;
		this.yJoueur = 100*tailleCaseEnPixel;

		//taille de la fen�tre de la carte adapt�e au nombre de pixels par case
		this.setPreferredSize(new Dimension(200*tailleCaseEnPixel,200*tailleCaseEnPixel));	
		this.layout = new JLayeredPane();
		this.add(layout);
		this.carte = carte;
	}


	/**
	 * composition de la carte avec les differentes images
	 */

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;


		int longueur = carte.getLongueur();
		int largeur = carte.getLargeur();


		// cases de fond: 
		for (int i = -45; i < 45 ; i++) {
			for (int j =-25; j < 25; j++) { 

				//dangereuse
				if (carte.getCases().get(xJoueur/tailleCaseEnPixel+i).get(yJoueur/tailleCaseEnPixel+j) instanceof Dangereuse) {
					g2d.setPaint(Color.darkGray);

					//chemin vers l'image
					ImageIcon danger = new ImageIcon("DONNEES/sand_rpg.png");
					Image imaged = danger.getImage();

					//affichage de l'image dans la case; on la redimensionne pour l'adapter a la case de cote tailleCaseEnPixel
					g2d.drawImage(imaged,(45+i)*tailleCaseEnPixel, (25+j)*tailleCaseEnPixel, (int) (imaged.getWidth(null)*0.126) ,(int) (imaged.getHeight(null)*0.126), null);
				}

				//sure herbe
				else if (carte.getCases().get(xJoueur/tailleCaseEnPixel+i).get(yJoueur/tailleCaseEnPixel+j) instanceof Herbe) {
					g2d.setPaint(Color.darkGray);
					ImageIcon grass = new ImageIcon("DONNEES/grassl_rpg.png"); 
					Image imageg = grass.getImage();
					g2d.drawImage(imageg,(45+i)*tailleCaseEnPixel, (25+j)*tailleCaseEnPixel, (int) (imageg.getWidth(null)*0.050) ,(int) (imageg.getHeight(null)*0.050), null);
				}
				//sure route
				else if (carte.getCases().get(xJoueur/tailleCaseEnPixel+i).get(yJoueur/tailleCaseEnPixel+j) instanceof Route) {
					g2d.setPaint(Color.darkGray);
					ImageIcon grass = new ImageIcon("DONNEES/road.png"); 
					Image imageg = grass.getImage();
					g2d.drawImage(imageg,(45+i)*tailleCaseEnPixel, (25+j)*tailleCaseEnPixel, (int) (imageg.getWidth(null)*0.0592) ,(int) (imageg.getHeight(null)*0.0592), null);
				}
				//impraticable mur
				else if (carte.getCases().get(xJoueur/tailleCaseEnPixel+i).get(yJoueur/tailleCaseEnPixel+j) instanceof Mur) {
					g2d.setPaint(Color.darkGray);
					ImageIcon grass = new ImageIcon("DONNEES/brick.png");
					Image imageg = grass.getImage();
					g2d.drawImage(imageg,(45+i)*tailleCaseEnPixel, (25+j)*tailleCaseEnPixel, (int) (imageg.getWidth(null)*0.068) ,(int) (imageg.getHeight(null)*0.068), null);
				}
				//sure plancher
				else if (carte.getCases().get(xJoueur/tailleCaseEnPixel+i).get(yJoueur/tailleCaseEnPixel+j) instanceof Plancher) {
					g2d.setPaint(Color.darkGray);
					ImageIcon grass = new ImageIcon("DONNEES/plancher.png"); 
					Image imageg = grass.getImage();
					g2d.drawImage(imageg,(45+i)*tailleCaseEnPixel, (25+j)*tailleCaseEnPixel, (int) (imageg.getWidth(null)*0.036) ,(int) (imageg.getHeight(null)*0.036), null);
				}
				//impraticable				
				else if (carte.getCases().get(xJoueur/tailleCaseEnPixel+i).get(yJoueur/tailleCaseEnPixel+j) instanceof Eau) {
					g2d.setPaint(Color.blue);
					ImageIcon palmier = new ImageIcon("DONNEES/water.png"); 
					Image imagee = palmier.getImage();
					g2d.drawImage(imagee,(45+i)*tailleCaseEnPixel, (25+j)*tailleCaseEnPixel, (int) (imagee.getWidth(null)*0.048) ,(int) (imagee.getHeight(null)*0.048), null);
				}




				if (((Case) carte.getCases().get(xJoueur/tailleCaseEnPixel+i).get(yJoueur/tailleCaseEnPixel+j)).getPersonnage() instanceof Dresseur) {
					ImageIcon img = new ImageIcon("DONNEES/pokemon_dresseur.png"); 
					Image image = img.getImage();
					g2d.drawImage(image,(45+i)*tailleCaseEnPixel, (25+j)*tailleCaseEnPixel, (int) (image.getWidth(null)*0.09) ,(int) (image.getHeight(null)*0.09), null);

				}



			}
		}


		//cases premier plan: arbre, pierre, batiment
		for (int i = -45; i < 45 ; i++) {
			for (int j =-25; j < 25; j++) { 


				if (carte.getCases().get(xJoueur/tailleCaseEnPixel+i).get(yJoueur/tailleCaseEnPixel+j) instanceof CentreSoins) {
					g2d.setPaint(Color.black);
					ImageIcon house = new ImageIcon("DONNEES/soigneur.png"); 
					Image imageh = house.getImage();
					g2d.drawImage(imageh,(45+i)*tailleCaseEnPixel-tailleCaseEnPixel/2, (j+24)*tailleCaseEnPixel, (int) (imageh.getWidth(null)*0.46) ,(int) (imageh.getHeight(null)*0.46), null);
				}

				else if (carte.getCases().get(xJoueur/tailleCaseEnPixel+i).get(yJoueur/tailleCaseEnPixel+j) instanceof Pierre) {
					g2d.setPaint(Color.gray);
					ImageIcon rock = new ImageIcon("DONNEES/stone.png"); 
					Image imager = rock.getImage();
					g2d.drawImage(imager,(45+i)*tailleCaseEnPixel-tailleCaseEnPixel/2, (25+j)*tailleCaseEnPixel-tailleCaseEnPixel/2, (int) (imager.getWidth(null)*0.10) ,(int) (imager.getHeight(null)*0.10), null);
				}
				else if (carte.getCases().get(xJoueur/tailleCaseEnPixel+i).get(yJoueur/tailleCaseEnPixel+j) instanceof Bois) {
					g2d.setPaint(Color.orange);
					ImageIcon palmier = new ImageIcon("DONNEES/tree_rpg.png");
					Image imagep = palmier.getImage();
					g2d.drawImage(imagep,(45+i)*tailleCaseEnPixel-tailleCaseEnPixel/2, (25+j)*tailleCaseEnPixel-tailleCaseEnPixel/2, (int) (imagep.getWidth(null)*0.4) ,(int) (imagep.getHeight(null)*0.4), null);

				}


			}
		}

		// guideur
		for (int i = 50; i < 150 ; i++) {
			for (int j = 50; j < 150; j++) { 


				if (carte.getCases().get(i).get(j) instanceof Dangereuse) {
					g2d.setPaint(Color.yellow);

				}
				else if (carte.getCases().get(i).get(j) instanceof Sure) {
					g2d.setPaint(Color.green);
				}

				else if (carte.getCases().get(i).get(j) instanceof Impraticable) {
					g2d.setPaint(Color.gray);
				}

				else if (carte.getCases().get(i).get(j) instanceof CentreSoins) {
					g2d.setPaint(Color.black);
				}

				g2d.fillRect(i*2, j*2,2,2);



			}
		}

		//deplacement du joueur sur la carte
		ImageIcon img = new ImageIcon("DONNEES/pokemon_player.png"); 
		Image image = img.getImage();
		g2d.drawImage(image,900, 495, (int) (image.getWidth(null)*0.09) ,(int) (image.getHeight(null)*0.1), null);


		//deplacement du pointeur rouge sur le guideur
		g2d.setPaint(Color.red);           
		g2d.fillRect(this.xJoueur/10,this.yJoueur/10, 6,6); 
	}

	public void addPanel(Component comp, int x, int y, int width,int height) {
		layout.add(comp, 1);
		comp.setBounds(x,y,width,height);
		comp.setVisible(true);
		this.refresh();
	}

	public void refresh() {
		this.revalidate();
		this.repaint();
	}

	public void removePanel(Component comp) {

		Timer timer = new Timer(3000, new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				remove(comp);
				refresh();
			}
		});
		timer.setRepeats(false);
		timer.start();
	}



	@Override
	public void keyPressed(KeyEvent e) {  // pour implementer KeyListener
		controleur.keyPressedInDeplacementPanel(e);
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public int getXJoueur() {
		return xJoueur;
	}

	public void setXJoueur(int xJoueur) {
		this.xJoueur = xJoueur;
	}

	public int getYJoueur() {
		return yJoueur;
	}

	public void setYJoueur(int yJoueur) {
		this.yJoueur = yJoueur;
	}



	public int getTailleCaseEnPixel() {
		return tailleCaseEnPixel;
	}



	public void setTailleCaseEnPixel(int tailleCaseEnPixel) {
		this.tailleCaseEnPixel = tailleCaseEnPixel;
	}

}