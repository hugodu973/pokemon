package InterfaceGraphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import controleur.Controleur;

/**
 * Classe permettant d'afficher plusieurs boutons et de renvoyer le choix fait par le joueur
 * @author
 */
public class ChoixPanel extends JPanel {
	private Controleur controleur;
	private ArrayList<String> possibilites;
	
	/**
	 * Constructeur de l'ensemble des boutons
	 * @param Controleur controleur
	 * @param ArrayList<String> possibilites: les divers boutons que le joueur pourra choisir
	 */
	public ChoixPanel(Controleur controleur, ArrayList<String> possibilites) {
		this.controleur = controleur;
		this.possibilites = possibilites;
		
		int len = possibilites.size();
		
		//creation des boutons de la liste possibilites
		this.setLayout(new FlowLayout());
		for (String s : possibilites) {
			Bouton temp = new Bouton(controleur,s);
			this.add(temp);
			temp.setPreferredSize(new Dimension(Math.max(150,s.length()*10),45));
			
		}
	}
	
}
