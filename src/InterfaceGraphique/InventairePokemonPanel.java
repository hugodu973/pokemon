package InterfaceGraphique;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import attaques.Attaque;
import controleur.Controleur;
import perso.Joueur;
import pokemon.Pokemon;
/**
 * Classe permettant la creation de l'interface de l'inventaire des pokemons
 * @author
 */
public class InventairePokemonPanel extends JPanel{
	private Controleur controleur;
	private Joueur p;
	
	/**
	 * Constructeur
	 * @param Controleur controleur
	 * @param Joueur p
	 */
	public InventairePokemonPanel(Controleur controleur,Joueur p) {
		this.controleur = controleur;
		this.p = p;
        
        this.setBorder(new EmptyBorder(new Insets(50, 50, 50, 50)));
        
        
        this.refresh();

	}
	
	/***
	 * Methode permettant la creation de l'interface graphique
	 */
	public void refresh() {
		
		//ajout d'un scroller pour faire defiler la fenetre
		JPanel scrollable = new JPanel();
		JScrollPane scroller = new JScrollPane(scrollable);
		scroller.setPreferredSize(new Dimension(1600,800));
		System.out.println();
		this.add(scroller,BorderLayout.CENTER);
		
		
		BoxLayout boxlayout = new BoxLayout(scrollable, BoxLayout.Y_AXIS);
	    scrollable.setLayout(boxlayout);
		
	    //ajout du bouton "Retour Menu" pour retourner au MenuPrincipal
		Bouton b = new Bouton(controleur,"Retour Menu");
		scrollable.add(b);
		
		scrollable.add(Box.createRigidArea(new Dimension(0, 60)));
		
		//affichage des pokemons present dans l'inventaire avec leurs attaques
		for (Pokemon pokemon : p.getInventairePokemon()) {
			JPanel poke = new JPanel();
			poke.setLayout(new BoxLayout(poke,BoxLayout.X_AXIS));
			
	        //image du pokemon avec sa barre de vie
			PokemonPanel combattantpane = new PokemonPanel(controleur,pokemon,0);
        	combattantpane.setBounds(0, 0, 100, 110);
        	
        	poke.add(combattantpane);
        	
        	//affichage des boutons attaques de chaque pokemons
        	ArrayList<String> liste = new ArrayList<String>();
        	for (Attaque a: pokemon.getAttaques()) {
        		liste.add(a.getNom());
        	}
        	
        	//utilisation de la classe ChoixPanel pour afficher les divers boutons crees
        	ChoixPanel attaques = new ChoixPanel(controleur,liste);
        	poke.add(attaques);
        	
        	//bouton pour ajouter un pokemon de l'inventaire vers l'equipe
        	if (p.getEquipe().size() != 1) {
        		BoutonEquipe choixEquipe = new BoutonEquipe(controleur,"ajouter a l'equipe",pokemon);
        		poke.add(choixEquipe);
        	}
        	
        	
        	scrollable.add(poke);
        	scrollable.add(Box.createRigidArea(new Dimension(0, 10))); 
     

        }
        revalidate();
        repaint();
	}

}
