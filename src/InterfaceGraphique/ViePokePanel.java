package InterfaceGraphique;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Combat.CombatVS;
import controleur.Controleur;
import pokemon.Pokemon;
/**
 * Classe permettant de visualiser la vie d'un pokemon, sous forme d'une barre coloree, lors de la phase de combat
 * @author
 *
 */
public class ViePokePanel extends JPanel {
	private Controleur controleur;
	private Pokemon combattant;
	
	/**
	 * Constructeur de l'interface pour visualiser la vie du pokemon
	 * @param controleur
	 * @param Pokemon combattant: pokemon dont on souhaite connaitre la vie 
	 */
	public ViePokePanel( Controleur controleur, Pokemon combattant) {
		this.controleur = controleur;
		this.combattant = combattant;
	}
	
	/**
	 * Methode permettant de reduire et modifier la couleur de la barre de vie selon les points de vie restants du pokemon
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		Pokemon poke = combattant;
		
		//calcul du rapport de la vie actuelle du pokemon sur le nombre maximal de points de vie
		double rapport = ((double) poke.getVie())/((double) poke.getVieMax());
		
		//en dessous du seuil critique de 0.3, l'affichage du niveau de vie devient rouge
		if ( rapport < 0.30) {
			g2d.setPaint(Color.red);
		}
		else if (rapport < 0.60) {
			g2d.setPaint(Color.yellow);
		}
		
		//par defaut la barre de vie est verte
		else {
			g2d.setPaint(Color.green);
		}
        
		//l'affichage du nombre de points de vie se fait sous forme d'une barre rectangulaire de longueur proportionnelle au nombre de points de vie restant
		g2d.fillRect( 10,55, (int) (rapport * 100),10);
		g2d.setPaint(Color.black);
		BasicStroke bs1 = new BasicStroke(1);
		g2d.setStroke(bs1);
		g2d.setFont(new Font("Serif",Font.PLAIN,25));
		g2d.drawRect(10,55, 100,10);
		g2d.drawString(poke.getNom(), 10, 20);
		g2d.drawString("niv : "+poke.getNiveau(), 10, 45);
			//
	}

}
