package InterfaceGraphique;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import controleur.Controleur;
/**
 * Classe permettant la creation du menu principale avec les boutons permettant d'acceder aux differentes interfaces du jeu 
 * @author
 *
 */
public class MenuPrincipalPanel extends JPanel {
	private Controleur controleur;

    /**
     * Constructeur du menu principal
     * @param Controleur controleur
     */
	public MenuPrincipalPanel(Controleur controleur) {
		this.controleur = controleur;
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		//liste comportant les textes des differents boutons a ajouter au menu
		ArrayList<String> list = new ArrayList<String>();
		list.add("Jouer");
		list.add("Equipe");
		list.add("Inventaire Pokemon");
		list.add("Sauvegarder");
		list.add("Charger");
		list.add("Quitter");
		
		//construction des différents boutons grace a la classe ChoixPanel
		ChoixPanel menu = new ChoixPanel(controleur,list);
		this.add(Box.createRigidArea(new Dimension(0, 300)));
		this.add(menu);
		this.setPreferredSize(new Dimension(500,500));
	}

}
