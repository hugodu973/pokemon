package InterfaceGraphique;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controleur.Controleur;
import pokemon.Pokemon;
/**
 * Classe permettant l'affichage graphique des pokemons avec leur niveau de vie lors de la phase de combat
 * @author
 *
 */
public class PokemonPanel extends JPanel{
	private Controleur controleur;
	private Pokemon combattant;
	
	/**
	 * Constructeur de la classe
	 * @param Controleur controleur
	 * @param Pokemon combattant: pokemon a afficher
	 * @param int orientation: permer de gerer l'orientation de l'image: 0 : combattant de gauche; 1 : combattant de droite
	 */
	public PokemonPanel( Controleur controleur, Pokemon combattant, int orientation) {
		this.controleur = controleur;
		this.combattant = combattant;
		
		BoxLayout boxlayout = new BoxLayout(this, BoxLayout.X_AXIS);
		this.setLayout(boxlayout);
		//interface graphique du nieau de vie de la classe ViePokePanel
		ViePokePanel stats = new ViePokePanel(controleur, this.combattant);
		
		//combattant de gauche
		if (orientation == 0) {
			JLabel lab = new JLabel(new ImageIcon("PokemonImages//" + combattant.getNom() + ".png"));
			//ajout a la fenetre d'affichage de l'image et du niveau de vie
			this.add(stats);
			this.add(lab);
		}
		//combattant de droite
		else if (orientation ==1) {
			JLabel lab = new JLabel(new ImageIcon("PokemonImages//"+ combattant.getNom() +"_mirror.png"));
			this.add(lab);
			this.add(stats);
			
		}
	}
	
}
