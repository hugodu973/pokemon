package InterfaceGraphique;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import controleur.Controleur;
/**
 * Classe permettant la cr�ation des boutons pour sauvegarder ou charger une partie sauvegardee
 * @author
 */
public class BoutonLoadAndSave extends JButton implements ActionListener{
	private Controleur control;
	private String filename;
	private int LoadOrSave;
	//0 : Sauvegarde
	//1 : Charger
	
	/**
	 * Constructeur du bouton de sauvegarde
	 * @param Controleur controleur
	 * @param String filename: chemin du fichier a charger
	 * @param int LoadOrSave: 0 pour sauvegarder; 1 pour charger la sauvegarde
	 */
	public BoutonLoadAndSave( Controleur controleur,String filename, int LoadOrSave) {
		super(filename);
		this.setPreferredSize(new Dimension(200,30));
		this.addActionListener(this);
		this.filename = filename;
		this.LoadOrSave = LoadOrSave;
		this.control = controleur;
	}
	
	/**
	 * M�thode permettant d'executer l'action du bouton: enregistrer ou sauvegarder
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		//sauvegarde
		if (LoadOrSave ==0) {
			System.out.println("CHECK SAUVEGARDE");
			control.sauvegarde(filename);
		}
		//chargement
		else if (LoadOrSave == 1) {
			System.out.println("CHECK CHARGEMENT");
			control.chargeSauvegarde(filename);
		}
	}

}
