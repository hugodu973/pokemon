package pokemon;

import java.util.Random;

import perso.Joueur;


/**
 * La classe PokeSauvage permet d'offrir une autre action possible aux pokemons sauvages : la fuite
 * @author formation
 *
 */
public class PokeSauvage extends Pokemon{
	
	
	/**
	 * Creation d'un pokemon sauvage de niveau proche de l'un des pokemons de l'equipe ( + ou - 5 niveaux )
	 * @param Joueur p
	 */
	public PokeSauvage(Joueur p) {
		super();
		Random r = new Random();
		int alea = r.nextInt(11);
		Pokemon pokeChoisi = p.getEquipe().get(r.nextInt(p.getEquipe().size()));
		this.setNiveau(Math.max(pokeChoisi.getNiveau()-5+ alea,1));
		while (this.getEvolutionSuivante() != null && this.getNiveau() >= this.getEvolutionSuivante().getNiveau()) {
			this.evolution();
		}
	}

	/**
	 * Creation d'un pokemon sauvage de nom et niveau definis
	 * @param String nom
	 * @param int niveau
	 */
	public PokeSauvage(String nom, int niveau) {
		super(nom, niveau);
		this.setEvolutionSuivante(null);
	}


	/**
	 * Methode qui permet de determiner si la tentative de fuite reussit
	 * @param int niveauPokemon1
	 * @param int niveauPokemon2
	 * @return boolean
	 */
	public boolean fuir(int niveauPokemon1,int niveauPokemon2) { 
		
		// On tire un nombre al�atoire
		double d = Math.random();
		
		// Fonction d�croissante lorsque le pokemon adverse est de haut niveau
		double seuil = niveauPokemon1/(Math.pow(niveauPokemon2, 1.3)); 
		
		if ( d < seuil) {
			return true;	
		}
		else {
			return false;
		}
	}
	

}
