package pokemon;

/**
 * Cette classe permet la creation de pokemons dresses
 * @author formation
 *
 */
public class PokeDresse extends Pokemon{

	// Creation d'un pokemon aleatoire
	public PokeDresse() {
		super();
	}
	
	// Generation du pokemon demande grace au fichier texte Pokemons
	public PokeDresse(String nom) {
		super(nom);
	}
	
	// Generation du pokemon demande et mise a jour de son niveau
	public PokeDresse(String nom, int niveau) {
		super( nom,  niveau);
	}
}

