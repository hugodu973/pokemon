package pokemon;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import Type.Acier;
import Type.Combat;
import Type.Eau;
import Type.Electricite;
import Type.Feu;
import Type.Normal;
import Type.Plante;
import Type.Poison;
import Type.Psy;
import Type.Roche;
import Type.Sol;
import Type.Type;
import Type.Vol;
import attaques.Att;
import attaques.AttPhysique;
import attaques.Attaque;
import attaques.Def;
import attaques.Esq;
import attaques.IBuffer;
import attaques.IDebuffer;

/**
 * La Classe pokemon permet la generation de pokemons et de gerer l'attaque (reussite, esquive, degats envoyes, re�us, etc..)
 * @author formation
 *
 */
public abstract class Pokemon {
	private int id;
	private String nom;
	private Type type;
	private ArrayList<Attaque> attaques;
	private Pokemon evolutionSuivante;
	private int probaEsquive;
	private int vie;
	private int att;
	private int def;
	private int vieMax;
	private int niveau;
	private HashMap<String,Type> dicoTypes;
	
	/**
	 * Methode qui genere aleatoirement un pokemon (nom, statistiques) mais pas son niveau qui est defini apres coup sur la base du niveau du joueur
	 */
	public Pokemon() {
		String[] poke = readerFichierPokemon();

		String[] listeAttaques = poke[2].split("\\,");

		this.id = 0;
		this.nom = poke[0];
		this.attaques = new ArrayList<Attaque>();
		
		for (String a : listeAttaques) {
			String[] natureAttaque = a.split("\\_");
			boolean isBuffer = false;
			if (natureAttaque[1].equals("phy")) {
				this.attaques.add(new AttPhysique(natureAttaque[0]));
			}
			else {
				if (natureAttaque[1].equals("buff")) {
					isBuffer = true;}
				else {
					isBuffer = false;
				}

				if (natureAttaque[2].equals("def")) {
					this.attaques.add(new Def(natureAttaque[0],isBuffer));
				}
				else if (natureAttaque[2].equals("att")) {
					this.attaques.add(new Att(natureAttaque[0],isBuffer));
				}
				else if (natureAttaque[2].equals("esq")) {
					this.attaques.add(new Esq(natureAttaque[0],isBuffer));
				}
			}

		}

		if (!poke[3].equals("null")) {
			this.setEvolutionSuivante(new PokeDresse(poke[3]));
		}
		else {
			this.setEvolutionSuivante(null);
		}
		this.probaEsquive = (int) (100*Double.parseDouble((poke[4])));
		this.vieMax = Integer.parseInt(poke[6]);
		this.vie = vieMax;
		this.att = Integer.parseInt(poke[7]);
		this.def = Integer.parseInt(poke[8]);
		this.niveau = 0;

		this.dicoTypes = new HashMap<String,Type>();
		this.dicoTypes.put("acier",new Acier());
		this.dicoTypes.put("combat",new Combat());
		this.dicoTypes.put("eau",new Eau());
		this.dicoTypes.put("electricite",new Electricite());
		this.dicoTypes.put("feu",new Feu());
		this.dicoTypes.put("normal",new Normal());
		this.dicoTypes.put("plante",new Plante());
		this.dicoTypes.put("poison",new Poison());
		this.dicoTypes.put("psy",new Psy());
		this.dicoTypes.put("roche",new Roche());
		this.dicoTypes.put("sol",new Sol());
		this.dicoTypes.put("vol",new Vol());
		this.type = dicoTypes.get(poke[1]);
	}

	/**
	 * Methode qui genere le pokemon demande en fonction du niveau renseigne en argument
	 * @param String nom
	 * @param int niveau
	 */
	public Pokemon(String nom, int niveau) {
		// renvoie un pokemon de niveau specifique
		//utilise pour : soigner les pokemon.
		//en les remplacant par une nouvelle instance
		this(nom);
		this.niveau = niveau;
	}
	
	/**
	 * Methode qui genere le pokemon reference du fichier Pokemon
	 * @param String nom
	 */
	public Pokemon (String nom) {
		
		String[] poke = readerFichierPokemon(nom);
		String[] listeAttaques = poke[2].split("\\,");
		this.id = 0;
		this.nom = nom;
		this.attaques = new ArrayList<Attaque>();	
		for (String a : listeAttaques) {
			String[] natureAttaque = a.split("\\_");
			boolean isBuffer = false;
			if (natureAttaque[1].equals("phy")) {
				this.attaques.add(new AttPhysique(natureAttaque[0]));
			}
			else {
				if (natureAttaque[1].equals("buff")) {
					isBuffer = true;}
				else {
					isBuffer = false;
				}

				if (natureAttaque[2].equals("def")) {
					this.attaques.add(new Def(natureAttaque[0],isBuffer));
				}
				else if (natureAttaque[2].equals("att")) {
					this.attaques.add(new Att(natureAttaque[0],isBuffer));
				}
				else if (natureAttaque[2].equals("esq")) {
					this.attaques.add(new Esq(natureAttaque[0],isBuffer));
				}
			}

		}
		
		this.probaEsquive = (int) (100*Double.parseDouble((poke[4])));
		this.vieMax = Integer.parseInt(poke[6]);
		this.vie = vieMax;
		this.att = Integer.parseInt(poke[7]);
		this.def = Integer.parseInt(poke[8]);
		
		// renvoie un pokemon du niveau minimal de leur stade d'evolution
		this.niveau = Integer.parseInt(poke[5]);
		
		if (!poke[3].equals("null")) {
			this.setEvolutionSuivante(new PokeDresse(poke[3]));
		}
		else {
			this.setEvolutionSuivante(null);
		}
		
		this.dicoTypes = new HashMap<String,Type>();
		this.dicoTypes.put("acier",new Acier());
		this.dicoTypes.put("combat",new Combat());
		this.dicoTypes.put("eau",new Eau());
		this.dicoTypes.put("electricite",new Electricite());
		this.dicoTypes.put("feu",new Feu());
		this.dicoTypes.put("normal",new Normal());
		this.dicoTypes.put("plante",new Plante());
		this.dicoTypes.put("poison",new Poison());
		this.dicoTypes.put("psy",new Psy());
		this.dicoTypes.put("roche",new Roche());
		this.dicoTypes.put("sol",new Sol());
		this.dicoTypes.put("vol",new Vol());
		this.type = dicoTypes.get(poke[1]);

	}
	
	/**
	 * Methode qui permet de lire le fichier de donnees sur les pokemons du jeu
	 * @param String nom
	 * @return String : contenu de la ligne concernant le pokemon voulu
	 */
	public String[] readerFichierPokemon(String nom) {
		String filePath = "FichierPokemon"; // chemin absolu vers le fichier
		Path p = Paths.get(filePath); // creation de l�objet Path

		try (BufferedReader reader = Files.newBufferedReader(p)) { // ouverture d�un buffer en lecture
			String line;
			line = reader.readLine().trim().replaceAll(" +", " ");
			String[] ligne= line.split("\\ ");
			
			while (!ligne[0].equals(nom)) {
				line = reader.readLine().trim().replaceAll(" +", " ");
				ligne= line.split("\\ ");
			}

			return ligne;

		} catch (IOException e) {
			e.printStackTrace();
			return new String[0];
		}	
	}
	
	/**
	 * Cette methode renvoie un pokemon aleatoire de niveau 1 ,plus tard, quand on fixe le niveau souhaite, on remplace le pokemon par l'evolution si besoin
	 * @return String[] ligne
	 */
	public String[] readerFichierPokemon() {
		
		int size = 0;
		String filePath = "FichierPokemon"; // chemin absolu vers le fichier
		Path p = Paths.get(filePath); // creation de lobjet Path
		String line;
		String linetrimmed;
		String[] ligne;
	
		try (BufferedReader reader = Files.newBufferedReader(p)) { // ouverture d�un buffer en lecture
			line = reader.readLine().trim().replaceAll(" +", " ");
			while (line != null) {
				linetrimmed = line.trim().replaceAll(" +", " ");
				ligne= linetrimmed.split("\\ ");
				
				
				if (ligne[5].equals("1")) {
					size += 1;
				}
				line = reader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Random r = new Random();
		int alea = r.nextInt(size);
	
		try (BufferedReader reader = Files.newBufferedReader(p)) { // ouverture d�un buffer en lecture
			int compteur = 0;
			line = reader.readLine().trim().replaceAll(" +", " ");
			ligne= line.split("\\ ");

			while (compteur != alea) {
				line = reader.readLine().trim().replaceAll(" +", " ");
				ligne= line.split("\\ ");
				if (ligne[5].equals("1")) {
					compteur +=1;
				}
			}
			return ligne;

		} catch (IOException e) {
			e.printStackTrace();
			return new String[0];
		}
	}


	/**
	 * Methode permettant d'attaquer
	 * @param att  statistique propre au pokemon 
	 * @return int : valeur des degats envoyes
	 */
	public int attaquer(Attaque att) {
	
		// Le pokemon peut rater chacune de ses attaques
		Random r = new Random();
		int nbAleatoire = r.nextInt(101);
		if (nbAleatoire < att.getProbaEchec()) {
			return 0;
		}
		
		// Si l'attaque est reussie
		else {
			if (att instanceof AttPhysique ) {
				// Cette fonction traite la valeur d'attaque statistique du pokemon et son niveau pour renvoyer les degats
				return (int) ((att.getValeur()*this.att*(((float)(2* this.niveau))/((float)5) +2))); 
			}
			
			// Si l'attaque utiliseee est un buff alors on ne renvoit aucun degat
			else if (!att.isBuffer())
			{
				return att.getValeur();
			}
			else {
				((IBuffer) att).buffer(this);
			}
			return 0;
		}
		
	}
	
	
	/**
	 * Cette methode permet au pokemon d'avoir une chance d'esquiver a chaque fois qu'il est attaque 
	 * @return int
	 */
	public boolean esquiver() {
		Random r = new Random();
		int nbAleatoire = r.nextInt(101);
		return (nbAleatoire < probaEsquive);
	}
	
	
	/**
	 * Cette methode traite les degats envoyes par le pokemon adverse et renvoie les degats recus par le pokemon attaque
	 * @param Attaque att
	 * @param int dgtsRecus
	 */
	public void recoitDegats(Attaque att, int dgtsRecus) {

		int degatsReduits = 0;
		if (att instanceof AttPhysique) {
			Type type = ((AttPhysique) att).getType();
			degatsReduits = (int) ((((float)dgtsRecus/((float)(this.def*7)))+2) * type.getReduc(this.type));
		}
		
		// Si l'attaque est un buff ou un debuff le pokemon ne subit aucun degat
		else if (!att.isBuffer()) {
			((IDebuffer) att).debuffer(this);
		}
		
		// La vie du pokemon est modifiee
		vie = vie-degatsReduits;
		
		// La vie du pokemon ne doit pas etre negative
		if (vie < 0) {
			vie = 0;
		}
	}

	
	/**
	 * Cette methode permet de faire evoluer le pokemon au stade superieur 
	 */
	public void evolution() {
		this.id = evolutionSuivante.getId();
		this.nom = evolutionSuivante.getNom();
		this.type = evolutionSuivante.getType();
		this.attaques = evolutionSuivante.getAttaques();	
		this.probaEsquive = evolutionSuivante.getProbaEsquive();
		this.vie = evolutionSuivante.getVie();
		this.att = evolutionSuivante.getAtt();
		this.def = evolutionSuivante.getDef();
		this.vieMax = evolutionSuivante.getVieMax(); 
	
		// Cette ligne est placee a la fin pour ne pas sauter un stade evolution
		this.evolutionSuivante = evolutionSuivante.getEvolutionSuivante(); 
	}
	
	
	/**
	 * Cette methode permet a un pokemon de gagner un niveau si il bat un pokemon de plus haut niveau
	 */
	public void gainXP() {
		this.niveau += 1;
	}

	
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getVie() {
		return vie;
	}
	public void setVie(int vie) {
		this.vie = vie;
	}
	public int getAtt() {
		return att;
	}
	public void setAtt(int att) {
		this.att = att;
	}
	public int getDef() {
		return def;
	}
	public void setDef(int def) {
		this.def = def;
	}
	public int getVieMax() {
		return vieMax;
	}
	public void setVieMax(int vieMax) {
		this.vieMax = vieMax;
	}
	public int getNiveau() {
		return niveau;
	}
	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}
	public ArrayList<Attaque> getAttaques() {
		return attaques;
	}
	public void setAttaques(ArrayList<Attaque> attaques) {
		this.attaques = attaques;
	}
	public Pokemon getEvolutionSuivante() {
		return evolutionSuivante;
	}
	public void setEvolutionSuivante(Pokemon p) {
		this.evolutionSuivante = p;
	}
	public int getProbaEsquive() {
		return probaEsquive;
	}
	public void setProbaEsquive(int probaEsquive) {
		this.probaEsquive = probaEsquive;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	
}
