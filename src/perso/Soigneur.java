package perso;

import java.util.ArrayList;

import deplacement.Case;
import pokemon.PokeDresse;
import pokemon.Pokemon;


/**
 * La classe Soigneur permet de creer des soigneurs qui peuvent soigner l'equipe du joueur
 * @author formation
 *
 */
public class Soigneur extends Personnage{
	
	/**
	 * Creation d'un soigneur
	 * @param int id
	 * @param String nom
	 * @param Case position
	 */
	public Soigneur(int id, String nom, Case position) {
		super(id, nom, position);
	}
	
	/**
	 * Methode soignant les pokemons de l'equipe du joueur
	 * @param ArrayList<Pokemon> equipe
	 */
	public void soigner(ArrayList<Pokemon> equipe) {
		for(Pokemon p : equipe) {
			p.setVie(p.getVieMax());
			PokeDresse temoin = new PokeDresse(p.getNom(),p.getNiveau());
			p.setAtt(temoin.getAtt());
			p.setDef(temoin.getDef());
			p.setProbaEsquive(temoin.getProbaEsquive());
		}
	}
}
