package perso;

import deplacement.Case;

/**
 * La classe personnage defini un personnage a une position
 * @author formation
 *
 */
public class Personnage {
	
	
	private int id;
	private String nom;
	private Case position;
	
		
	/**
	 * Creation du personnage sur une case
	 * @param int id
	 * @param String nom
	 * @param Case position
	 */
	public Personnage(int id, String nom, Case position) {
		this.id = id;
		this.nom = nom;
		this.position = position;
	}


	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Case getPosition() {
		return position;
	}
	public void setPosition(Case position) {
		this.position = position;
	}

}
