package perso;

import java.util.ArrayList;
import java.util.Random;

import deplacement.Case;
import deplacement.Grille;
import deplacement.Impraticable;
import pokemon.PokeDresse;
import pokemon.Pokemon;


/**
 * Cette classe permet de generer des dresseurs qui ont la possibilite de se deplacer sur la grille et des equipes de pokemons de niveau equilibre
 * @author formation
 *
 */
public class Dresseur extends Personnage{
	
	private  ArrayList<Pokemon> equipe;
	
	/**
	 * Creation d'un dresseur et de son equipe
	 * @param int id
	 * @param String nom
	 * @param Case position
	 */
	public Dresseur(int id, String nom, Case position) {
		super(id, nom, position);
		this.equipe = new ArrayList<Pokemon>();
		int i=0;
		while (i<6) {
			// Confere au dresseur 6 pokemons tires aleatoirement
			Pokemon poke = new PokeDresse();
			if (this.equipe.contains(poke) == false) {
				this.equipe.add(poke);
				i++;
			}
		}
	}
	
	/**
	 * Methode qui donne aux pokemons du dresseur un niveau proche de l'un des pokemons de l'equipe du joueur (+ ou - 5 niveau)
	 * @param Joueur p
	 */
	public void equilibrageNiveau(Joueur p) {
		for (Pokemon poke : equipe) {
			Random r = new Random();
			int alea = r.nextInt(11);
			Pokemon pokeChoisi = p.getEquipe().get(r.nextInt(p.getEquipe().size()));
			poke.setNiveau(Math.max(1,pokeChoisi.getNiveau()-5+ alea));
			
			//si le niveau donne est suffisant pour evoluer, on remplace le pokemon
			while (poke.getEvolutionSuivante() != null && poke.getNiveau() >= poke.getEvolutionSuivante().getNiveau()) {
				poke.evolution();
			}
		}
	}
	
	/**
	 * Methode de deplacement du joueur vers la gauche sur la grille
	 * @param Grille grille
	 */
	public void deplacergauche(Grille grille) {
		int col = this.getPosition().getX();
		int lig = this.getPosition().getY();
		Case voisinGauche = grille.getCases().get(col-1).get(lig);
		if (!( voisinGauche instanceof Impraticable)) {
			this.setPosition(voisinGauche);
		}
	}
	
	/**
	 * Methode de deplacement du joueur vers la droite sur la grille
	 * @param Grille grille
	 */
	public void deplacerdroite(Grille grille) {
		int col = this.getPosition().getX();
		int lig = this.getPosition().getY();
		Case voisinDroite = grille.getCases().get(col+1).get(lig);
		if (!( voisinDroite instanceof Impraticable)) {
			this.setPosition(voisinDroite);
		}
	}
	
	/**
	 * Methode de deplacement du joueur vers le haut sur la grille
	 * @param grille
	 */
	public void deplacerhaut(Grille grille) {
		int col = this.getPosition().getX();
		int lig = this.getPosition().getY();		Case voisinHaut = grille.getCases().get(col).get(lig-1);
		if (!(voisinHaut instanceof Impraticable)) {
			this.setPosition(voisinHaut);
		}
	}
	
	/**
	 * Methode de deplacement du joueur vers le bas sur la grille
	 * @param grille
	 */
	public void deplacerbas(Grille grille) {
		int col = this.getPosition().getX();
		int lig = this.getPosition().getY();
		Case voisinBas = grille.getCases().get(col).get(lig+1);
		if (!(voisinBas instanceof Impraticable)) {
			this.setPosition(voisinBas);
		}
	}
	
	
	public ArrayList<Pokemon> getEquipe() {
		return equipe;
	}
	public void setEquipe(ArrayList<Pokemon> equipe) {
		this.equipe = equipe;
	}
}



