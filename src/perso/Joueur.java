package perso;

import java.util.ArrayList;

import deplacement.Case;
import inventaire.AttrapeBall;
import inventaire.Objet;
import inventaire.Pokeball;
import inventaire.Potion1;
import pokemon.Pokemon;


/**
 * La classe Joueur permet au joueur de gerer son inventaire de fuir un combat et d'interagir avec des PNJ
 * @author formation
 *
 */
public class Joueur extends Dresseur{
	
	private ArrayList<Objet> inventaire;
	private ArrayList<Pokemon> inventairePokemon;
	private Pokemon pokemonParDefaut;
	
	/**
	 * Confere un inventaire et des pokemons a un joueur
	 * @param int id
	 * @param String nom
	 * @param Case position
	 * @param ArrayList<Objet> inventaire
	 * @param ArrayList<Pokemon> inventairePokemon
	 */
	public Joueur(int id, String nom, Case position, ArrayList<Objet> inventaire,
			ArrayList<Pokemon> inventairePokemon) {
		super(id, nom, position);
		this.inventaire = inventaire;
		this.inventairePokemon = inventairePokemon;
	}
	
	/**
	 * Generation de l'inventaire initial
	 * @param int id
	 * @param String nom
	 * @param Case position
	 */
	public Joueur(int id, String nom, Case position) {
		super(id, nom, position);
		
		//inventaire par defaut: 10 pokeballs et 5 potions de soin
		this.inventaire = new ArrayList<Objet>();
		for(int i=0; i<10; i++) {
			this.inventaire.add(new Pokeball());
		}
		for(int i=0; i<5; i++) {
			this.inventaire.add(new Potion1());
		}
		
		//inventaire pokemon par defaut: vide
		this.setEquipe(new ArrayList<Pokemon>());
		this.inventairePokemon = new  ArrayList<Pokemon>();
	}
	
	/**
	 * Methode permettant de capturer des pokemons sauvages
	 * @param AttrapeBall attrapeball
	 * @param Pokemon pok
	 * @return boolean
	 */
	public boolean capturer(AttrapeBall attrapeball, Pokemon pok) {
		
		double d = Math.random();
		// Fonction prenant en compte l'affaiblissement  du pokemon
		double taux_capture = attrapeball.getProba_capture() / (2*(pok.getVie() / pok.getVieMax())); 
		
		// La pokeball est supprimee des qu'elle est utilisee
		inventaire.remove(attrapeball);
		if (d < taux_capture) {
			this.inventairePokemon.add(pok);
			return true;
		}
		else {
			return false;
		}		
	}

	/**
	 * Methode permettant au joueur de fuir en combat
	 * @param int niveauPokemon1
	 * @param int niveauPokemon2
	 * @return boolean
	 */
	public boolean fuir(int niveauPokemon1,int niveauPokemon2) {
		double d = Math.random();
		// Fonction prenant en compte le niveau du pokemon du joueur et celui du pokemon adverse
		double seuil = niveauPokemon1/(Math.pow(niveauPokemon2, 1.3));
		
		if ( d < seuil) {
			return true;	
		}
		else {
			return false;
		}
	}
	
	/**
	 * Methode permettant d'interagir avec un PNJ lorsque le joueur est sur sa case
	 * @param Case c
	 * @return String
	 */
	public String interagir(Case c) {
		//Active la methode du NPC (soigne ou Combat)
		if (c.getPersonnage() instanceof Soigneur) {
			((Soigneur) c.getPersonnage()).soigner(this.getEquipe());	
			return "Soin";
		}
		else if (c.getPersonnage() instanceof Dresseur) {
			return "Combat";
		}
		return "";
	}
	
	/**
	 * Utilisation d'un objet de l'inventaire
	 * @param Objet objet
	 * @param Pokemon pokemon
	 */
	public void utiliserObjet(Objet objet, Pokemon pokemon) {
			objet.utiliser(pokemon);
			inventaire.remove(objet);
	}
	
	/**
	 * Selection du pokemon par defaut 
	 * @param Pokemon pokemon
	 */
	public void selectionPokemonParDefaut (Pokemon pokemon) {
		if (this.getEquipe().contains(pokemon)) {
			this.pokemonParDefaut = pokemon;
		}
	}

	
	public ArrayList<Objet> getInventaire() {
		return inventaire;
	}
	public void setInventaire(ArrayList<Objet> inventaire) {
		this.inventaire = inventaire;
	}
	public ArrayList<Pokemon> getInventairePokemon() {
		return inventairePokemon;
	}
	public void setInventairePokemon(ArrayList<Pokemon> inventairePokemon) {
		this.inventairePokemon = inventairePokemon;
	}
	public Pokemon getPokemonParDefaut() {
		return pokemonParDefaut;
	}
	public void setPokemonParDefaut(Pokemon pokemonParDefaut) {
		this.pokemonParDefaut = pokemonParDefaut;
	}
}