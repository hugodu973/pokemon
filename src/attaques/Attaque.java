package attaques;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import Type.Acier;
import Type.Combat;
import Type.Eau;
import Type.Electricite;
import Type.Feu;
import Type.Normal;
import Type.Plante;
import Type.Poison;
import Type.Psy;
import Type.Roche;
import Type.Sol;
import Type.Type;
import Type.Vol;

public abstract class Attaque {
	private int id;
	private String nom;
	private int probaEchec;
	private int valeur;
	private boolean isBuffer;
	private static HashMap<String, Type> dicoTypeAttaques;
	
	/**
	 * Création d une attaque et definition de ses attributs
	 * @param nom
	 * @param boolean isBuffer
	 */
	public Attaque(String nom, boolean isBuffer) {
		String[] att = readerFichierAttaque(nom);
		this.id = 0;
		this.nom = nom;
		this.probaEchec = (int) (100*Double.parseDouble(att[2]));
		this.valeur = Integer.parseInt(att[1]);
		this.isBuffer = isBuffer;
		this.dicoTypeAttaques = new HashMap<String,Type>();
		this.dicoTypeAttaques.put("acier",new Acier());
		this.dicoTypeAttaques.put("combat",new Combat());
		this.dicoTypeAttaques.put("eau",new Eau());
		this.dicoTypeAttaques.put("electricite",new Electricite());
		this.dicoTypeAttaques.put("feu",new Feu());
		this.dicoTypeAttaques.put("normal",new Normal());
		this.dicoTypeAttaques.put("plante",new Plante());
		this.dicoTypeAttaques.put("poison",new Poison());
		this.dicoTypeAttaques.put("psy",new Psy());
		this.dicoTypeAttaques.put("roche",new Roche());
		this.dicoTypeAttaques.put("sol",new Sol());
		this.dicoTypeAttaques.put("vol",new Vol());

	}
	
	/**
	 * Création d une attaque du fichier attaques
	 * @param String nom
	 * @return String[]
	 */
	public String[] readerFichierAttaque(String nom) {
		String filePath = "FichierAttaques"; // chemin absolu vers le fichier
		Path p = Paths.get(filePath); // creation de lobjet Path

		try (BufferedReader reader = Files.newBufferedReader(p)) { // ouverture dun buffer en lecture
			String line;
			line = reader.readLine().trim().replaceAll(" +", " ");
			String[] ligne= line.split("\\ ");
			
			while (!ligne[0].equals(nom)) {
				line = (reader.readLine()).trim().replaceAll(" +", " ");
				ligne= line.split("\\ ");
			}
			return ligne;

		} catch (IOException e) {
			e.printStackTrace();
			return new String[0];
		}
		
	}
	
	public int getProbaEchec() {
		return probaEchec;
	}
	public int getValeur() {
		return valeur;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public static HashMap<String, Type> getDicoTypeAttaques() {
		return dicoTypeAttaques;
	}
	public static void setDicoTypeAttaques(HashMap<String, Type> dicoTypeAttaques) {
		Attaque.dicoTypeAttaques = dicoTypeAttaques;
	}
	public void setProbaEchec(int probaEchec) {
		this.probaEchec = probaEchec;
	}
	public void setValeur(int valeur) {
		this.valeur = valeur;
	}
	public boolean isBuffer() {
		return isBuffer;
	}
	public void setBuffer(boolean isBuffer) {
		this.isBuffer = isBuffer;
	}
	
}