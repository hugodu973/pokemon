package attaques;

import pokemon.Pokemon;

/**
 * Cette classe permet de gerer les buffs et debuffs concernant l attaque lors d un combat
 * @author formation
 *
 */
public class Att extends Attaque implements IBuffer,IDebuffer{

	/**
	 * Defini une attaque grace a un nom et la nature buff
	 * @param String nom
	 * @param boolean isBuffer
	 */
	public Att(String nom, boolean isBuffer) {
		super(nom, isBuffer);
		
	}

	@Override
	public void buffer(Pokemon poke) {
		poke.setAtt( poke.getAtt() + this.getValeur());
	}

	@Override
	public void debuffer(Pokemon poke) {
		poke.setAtt(poke.getAtt() - this.getValeur());
	}

}