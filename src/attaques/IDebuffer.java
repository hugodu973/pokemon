package attaques;

import pokemon.Pokemon;

public interface IDebuffer {
	
	/**
	 *  Redefini par la suite dans les classes Att Def et Esq pour modifier l attribut voulu
	 * @param Pokemon poke
	 */
	public abstract void debuffer(Pokemon poke);
	
}