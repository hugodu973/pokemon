package attaques;

import Type.Type;

/**
 * Cette classe permet de gerer les attaques physiques lors d'un combat
 * @author formation
 *
 */
public class AttPhysique extends Attaque{
	
	private Type type;
	
	/**
	 * Defini une attaque physique grace a un nom et la nature attaque physique
	 * @param String nom
	 */
	public AttPhysique(String nom) {
		super(nom, false);
		String[] ligne = super.readerFichierAttaque(nom);
		this.type = this.getDicoTypeAttaques().get((ligne[3]));
	}
	
	/**
	 * Permet d'obtenir le type de l'attaque
	 * @return Type type
	 */
	public Type getType() {
		return this.type;
	}
}