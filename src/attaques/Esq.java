package attaques;

import pokemon.Pokemon;

public class Esq extends Attaque implements IBuffer,IDebuffer{
	
	
	/**
	 * Defini une attaque grace a un nom et la nature buff
	 * @param String nom
	 * @param boolean isBuffer
	 */
	public Esq(String nom, boolean isBuffer) {
		super(nom, isBuffer);
		}

	@Override
	public void buffer(Pokemon poke) {
		poke.setProbaEsquive( poke.getProbaEsquive() + this.getValeur());
	}

	@Override
	public void debuffer(Pokemon poke) {
		poke.setProbaEsquive(poke.getProbaEsquive() - this.getValeur());
	}
	

	
}