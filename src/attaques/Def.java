package attaques;

import pokemon.Pokemon;

/**
 * Cette classe permet de gerer les buffs et debuffs concernat la defense lors d'un combat
 * @author formation
 *
 */
public class Def extends Attaque implements IBuffer,IDebuffer{

	/**
	 * Defini une attaque grace a un nom et la nature buff
	 * @param String nom
	 * @param boolean isBuffer
	 */
	public Def(String nom, boolean isBuffer) {
		super(nom, isBuffer);
	}

	@Override
	public void buffer(Pokemon poke) {
		poke.setDef( poke.getDef() + this.getValeur());
	}

	@Override
	public void debuffer(Pokemon poke) {
		poke.setDef(poke.getDef() - this.getValeur());
	}
	
}