package inventaire;
/**
 * Creation d'un type d'attrapeball: pokeball de probabilité 0.1
 * @author formation
 *
 */
public class Pokeball extends AttrapeBall{
	
	public Pokeball() {
		super("Pokeball");
		this.setProba_capture(0.1);
	}

}
