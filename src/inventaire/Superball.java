package inventaire;
/**
 * Creation d'un type d'attrapeball: Sueperball de probabilité 0.2
 * @author formation
 *
 */
public class Superball extends AttrapeBall{
	
	public Superball() {
		super("Superball");
		this.setProba_capture(0.2);
	}

}
