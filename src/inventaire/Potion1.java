package inventaire;
/**
 * Creation d'un type de potion: donne 5 points de soin
 * @author
 */
public class Potion1 extends PotionSoin{

	public Potion1() {
		super("Potion magique du druide", 5);
	}
}
