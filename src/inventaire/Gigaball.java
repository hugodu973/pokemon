package inventaire;

import pokemon.Pokemon;
/**
 * Creation d'un type d'attrapeball: Gigaball de proba 0.4
 * @author 
 */
public class Gigaball extends AttrapeBall{

	public Gigaball() {
		super("Gigaball");
		this.setProba_capture(0.4);
	}

	
	
}
