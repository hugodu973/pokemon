package inventaire;

import java.util.ArrayList;

import pokemon.Pokemon;
/**
 * Classe abstraite permettant la creation des objets: potions de soins et attrapeballs
 * @author  
 */
public abstract class Objet {
	
	
	private String nom;
	private String description;
	private ArrayList<Objet> inventaire;
	
	/**
	 * Methode permettant d'utiliser l'objet sur un pokemon
	 * @param Pokemon pokemon: pokemon sur lequel on utilise l'objet
	 */
	public void utiliser(Pokemon pokemon) {
		
	}
	
	/**
	 * Constructeur de l'objet avec nom et description
	 * @param nom
	 * @param description
	 */
	public Objet(String nom, String description) {
		this.nom = nom;
		this.description = description;
	}
	 
	/**
	  * Constructeur de l'objet avec nom
	  * @param nom
	  */
	public Objet(String nom) {
		this.nom = nom;
		this.description = "Inconnue";
	}

	public String getNom() {
		return nom;
	}
	public ArrayList<Objet> getInventaire() {
		return inventaire;
	}

	public void setInventaire(ArrayList<Objet> inventaire) {
		this.inventaire = inventaire;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
