package inventaire;

import pokemon.Pokemon;
/**
 * Classe permettant la creation des potions de soins
 * @author 
 *
 */
public abstract class PotionSoin extends Objet{
	private int nbreSoin;

 
	/**
	 * Constructeur d'une potion de soin
	 * @param String nom
	 * @param String nbreSoin
	 */
	public PotionSoin(String nom, int nbreSoin) {
		super(nom, "Permet de soigner un pokemon");
		this.nbreSoin = nbreSoin;
	}
	
	@Override
	public void utiliser(Pokemon pokemon) {
		//Cas ou le nombre de soins de la potion depasse le nombre maximum de points de vie: le pokemon regagne sa vie max
		if (pokemon.getVieMax()-pokemon.getVie() < this.nbreSoin){
			pokemon.setVie(pokemon.getVieMax());
		}
		//Cas general: le pokemon regagne nbreSoin comme point de vie
		else {
			pokemon.setVie(pokemon.getVie()+this.nbreSoin);			
		}	
	}
	
	public int getNbreSoin() {
		return nbreSoin;
	}

	public void setNbreSoin(int nbreSoin) {
		this.nbreSoin = nbreSoin;
	}

	
}
