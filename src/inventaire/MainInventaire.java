package inventaire;

import deplacement.Case;
import deplacement.Sure;
import perso.Dresseur;
import perso.Joueur;

public class MainInventaire {
	
	public static void main(String[] args) {
		
		Gigaball g1 = new Gigaball();
		System.out.println(g1.getDescription());
		System.out.println(g1.getNom());
		System.out.println(g1.getProba_capture());
		
		Case c1 = new Sure(0,0);
		Joueur p1 = new Joueur(3, "Max", c1 );
		Dresseur p2 = new Dresseur(3, "Max", c1 );
		
		System.out.println(p1.getClass());
		System.out.println(p2.getClass());
		
	}

}
