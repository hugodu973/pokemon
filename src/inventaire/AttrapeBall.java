package inventaire;

import pokemon.Pokemon;
/**
 * Classe permettant la creation des attrapeballs
 * @author 
 */
public abstract class AttrapeBall extends Objet{
	private double proba_capture;
	
	
	public double getProba_capture() {
		return proba_capture;
	}

	public void setProba_capture(double proba_capture) {
		this.proba_capture = proba_capture;
	}

	/**
	 * Constructeur d'une attrapeball, avec une probabilité de capture
	 * @param String nom
	 * @param double proba_capture: probabilite de capturer le pokemon
	 */
	public AttrapeBall(String nom, double proba_capture) {
		super(nom);
		this.proba_capture = proba_capture;
	}
	
	/**
	 * Constructeur d'une attrapeball
	 * @param String nom
	 */
	public AttrapeBall(String nom) {
		super(nom,"Permet de capturer un pokemon");
	}
	
}

	
	
	
	
	
	
	
