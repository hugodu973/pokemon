package inventaire;
/**
 * Creation d'un type d'attrapeball: Megaball de probabilite 0.3
 * @author 
 *
 */
public class Megaball extends AttrapeBall {
	
	public Megaball() {
		super("Megaball");
		this.setProba_capture(0.3);
	}

}
